import React, { Component } from "react";
import http from "../../services/httpService";
import auth from "../../services/authService";

class DepositCheque extends Component {
	state = {
		cheque: {},
		banks: [],
		errors: {
			chequeNumber: "pop",
			amount: "pop",
			branch: "pop",
			bankName: "pop",
		},
	};
	async componentDidMount() {
		let user = auth.getUser();
		let response = await http.get("/getBanks");
		let { data } = response;
		let s1 = { ...this.state };
		s1.banks = data;
		s1.cheque.name = user.name;
		this.setState(s1);
	}
	async postData(url, obj) {
		let response = await http.post(url, obj);
		this.props.history.push("/");
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.cheque[input.name] = input.value;
		let error = this.validateInput(s1.cheque[input.name], input.name);
		s1.errors[input.name] = error;
		this.setState(s1);
	};
	validateInput = (inp, name) => {
		let error = "";
		if (name === "chequeNumber") {
			error = this.checkChequeNumber(inp);
		} else if (name === "branch") {
			error = this.checkBranch(inp);
		} else {
			error = this.checkInput(inp);
		}
		return error;
	};
	checkBranch = (val) =>
		val
			? val.length > 3
				? ""
				: "Enter 4 digit code of branch"
			: "Enter 4 digit code of branch";
	checkInput = (val) => (val ? "" : "Cannot be Empty");
	checkChequeNumber = (val) =>
		val
			? val.length > 10
				? ""
				: "Enter your 11 digit Cheque Number"
			: "Enter your 11 digit Cheque Number";
	makeInputField = (label, name, ph) => {
		return (
			<React.Fragment>
				<label style={{ fontSize: "19px" }}>
					{label}
					<span className='text-danger'>*</span>
				</label>
				<input
					className='form-control'
					type={
						name === "amount" || name === "chequeNumber" ? "number" : "text"
					}
					name={name}
					placeholder={` Enter ${ph}`}
					onChange={this.handleChange}
				></input>
			</React.Fragment>
		);
	};
	makeDD = (arr, sel, label, name) => {
		return (
			<select
				className='form-control'
				value={sel}
				name={name}
				onChange={this.handleChange}
			>
				<option>Select {label}</option>
				{arr.map((ele) => {
					return (
						<option value={name === "month" ? ele.substring(0, 3) : ele}>
							{ele}
						</option>
					);
				})}
			</select>
		);
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		if (this.validateErrors(s1.errors)) {
			alert("details Added Successfully");
			this.postData("/postCheque", s1.cheque);
		} else alert("Enter all the details correctly");
	};

	render() {
		let { banks, errors, cheque } = this.state;
		let { bankName, branch, amount, chequeNumber } = cheque;
		return (
			<div className='container mt-4'>
				<h3>Deposit Cheque</h3>
				<div className='form-group'>
					{this.makeInputField(
						"Cheque Number",
						"chequeNumber",
						"Cheque Number"
					)}
					{errors.chequeNumber && errors.chequeNumber !== "pop" && (
						<span
							className='form-control'
							style={{ background: "pink", color: "#a5002f" }}
						>
							{errors.chequeNumber}
						</span>
					)}
				</div>
				<hr></hr>
				<div className='form-group'>
					<label style={{ fontSize: "19px" }}>
						Bank Name<span className='text-danger'>*</span>
					</label>
					{this.makeDD(banks, bankName, "bank", "bankName")}
				</div>
				<hr></hr>
				<div className='form-group'>
					{this.makeInputField("Branch", "branch", "Branch Code")}
					{errors.branch && errors.branch !== "pop" && (
						<span
							className='form-control'
							style={{ background: "pink", color: "#a5002f" }}
						>
							{errors.branch}
						</span>
					)}
				</div>
				{this.makeInputField("Amount", "amount", "Amount")}
				<hr></hr>
				<button className='btn btn-primary' onClick={this.handleSubmit}>
					Add Cheque
				</button>
			</div>
		);
	}
}
export default DepositCheque;
