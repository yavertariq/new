import React, { Component } from "react";
import http from "../../services/httpService";
import auth from "../../services/authService";

class Login extends Component {
	state = {
		user: { name: "", password: "" },
		errors: { name: "pop", password: "pop" },
		submit: true,
	};
	async postData(url, obj) {
		try {
			let response = await http.post(url, obj);
			let { data } = response;
			auth.login(data);
			console.log(data);
			data.role === "manager"
				? (window.location = "/admin")
				: (window.location = "/customer");
		} catch (ex) {
			if (
				ex.response &&
				(ex.response.status === 400 || ex.response.status === 500)
			) {
				let error = "Check Username or password";
				this.setState({ error: error });
			}
		}
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.user[input.name] = input.value;
		let error = this.validateInput(s1.user, input.name);
		s1.errors[input.name] = error;
		if (this.validateErrors(s1.errors)) {
			s1.submit = false;
		} else {
			s1.submit = true;
		}
		this.setState(s1);
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	validateInput = (user, fname) => {
		let { name, password } = user;
		let error = "";
		if (fname === "name") {
			error = this.checkUsername(name);
		} else {
			error = this.checkPassword(password);
		}
		return error;
	};
	checkUsername = (str) => (str ? "" : "User Name cannot be Empty");
	checkPassword = (str) =>
		str
			? str.length < 7
				? "Password must be 7 character"
				: ""
			: "Password must be 7 character";
	handleSubmit = (e) => {
		e.preventDefault();
		this.postData(`/login`, this.state.user);
	};
	render() {
		let { user, errors, submit, error } = this.state;
		let { name, password } = user;
		return (
			<div className='container text-center mt-5'>
				<h2>Welcome to GBI Bank</h2>
				<div className='row mt-5'>
					<div className='col-12'>
						{error && <span className='text-danger'>{error}</span>}
					</div>
					<div className='col-12'>
						<h5>User Name</h5>
					</div>
					<div className='col-3'></div>
					<div className='col-6'>
						<input
							className='form-control'
							name='name'
							placeholder=' Enter User Name'
							value={name}
							onChange={this.handleChange}
						></input>
					</div>
					<div className='col-3'></div>
					<span className='col-12 text-muted'>
						We'll not share your user name with anyone else
					</span>
					<div className='col-12 mt-4'>
						<h5>Password</h5>
					</div>
					<div className='col-3'></div>
					<div className='col-6'>
						<input
							className='form-control'
							type='password'
							name='password'
							placeholder=' Password'
							value={password}
							onChange={this.handleChange}
						></input>
						{errors.password && errors.password !== "pop" ? (
							<span className='text-danger'>{errors.password}</span>
						) : (
							""
						)}
					</div>
					<div className='col-3'></div>
				</div>
				<button
					className='btn btn-primary mt-4'
					disabled={submit}
					onClick={this.handleSubmit}
				>
					Login
				</button>
			</div>
		);
	}
}
export default Login;
