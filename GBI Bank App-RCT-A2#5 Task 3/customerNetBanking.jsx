import React, { Component } from "react";
import http from "../../services/httpService";
import auth from "../../services/authService";
import queryString from "query-string";

class ViewNetBanking extends Component {
	state = {
		data: {},
	};
	async componentDidMount() {
		let queryParams = queryString.parse(this.props.location.search);
		let { page } = queryParams;
		let user = auth.getUser();
		let response = await http.get(
			`/getNetBankingByName/${user.name}?page=${page}`
		);
		let { data } = response;
		console.log(data);
		this.setState({ data: data });
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.componentDidMount();
	}
	handlePageChange = (val) => {
		let queryParams = queryString.parse(this.props.location.search);
		let newPage = +queryParams.page + val;
		queryParams.page = newPage;
		this.props.history.push(`/viewNet?page=${newPage}`);
	};
	render() {
		let { data } = this.state;
		let { items = [], page, totalItems, totalNum } = data;
		let end = page * 5;
		let start = end - 4;
		if (end > totalNum) {
			end = totalNum;
		}
		return (
			<div className='container mt-5'>
				<h2>All Net Banking Details</h2>
				{items.length > 0 ? (
					<React.Fragment>
						<h5>
							{start} - {end} of {totalNum}
						</h5>
						<div
							className={
								"row pt-4 pb-4 text-center font-weight-bold border-top border-bottom"
							}
						>
							<div className='col-4'>Payee Name</div>
							<div className='col-2'>Amount</div>
							<div className='col-2'>Bank Name</div>
							<div className='col-4'>Comment</div>
						</div>
						{items ? (
							<React.Fragment>
								{items.map((ele, index) => {
									return (
										<div
											className={
												"row pt-4 pb-4 text-center " +
												(index % 2 === 0
													? "bg-light border-top border-bottom"
													: "")
											}
										>
											<div className='col-4'>{ele.payeeName}</div>
											<div className='col-2'>{ele.amount}</div>
											<div className='col-2'>{ele.bankName}</div>
											<div className='col-4'>{ele.comment}</div>
										</div>
									);
								})}
								{page > 1 ? (
									<button
										className='btn btn-secondary mt-3'
										onClick={() => this.handlePageChange(-1)}
									>
										Prev
									</button>
								) : (
									""
								)}
								{end === totalNum ? (
									""
								) : (
									<button
										className='btn btn-secondary mt-3 float-right'
										onClick={() => this.handlePageChange(1)}
									>
										Next
									</button>
								)}
							</React.Fragment>
						) : (
							""
						)}
					</React.Fragment>
				) : end === 0 ? (
					<h4 style={{ color: "#df3666" }}>No Transactions to show</h4>
				) : (
					""
				)}
			</div>
		);
	}
}
export default ViewNetBanking;
