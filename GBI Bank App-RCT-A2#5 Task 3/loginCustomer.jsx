import React, { Component } from "react";
import bank from "./bank.jpg";

class LoginCustomer extends Component {
	render() {
		return (
			<div className='container text-center'>
				<h2 style={{ color: "#C70039", margin: "40px 0" }}>
					Welcome to GBI Bank customer portal
				</h2>
				<img src={bank}></img>
			</div>
		);
	}
}
export default LoginCustomer;
