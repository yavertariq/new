import React, { Component } from "react";
import http from "../../services/httpService";
import queryString from "query-string";
import LeftPanel from "./leftPanel";

class AllCheques extends Component {
	state = {
		data: {},
	};
	async componentDidMount() {
		let queryParams = queryString.parse(this.props.location.search);
		let searchStr = this.makeSearchString(queryParams);
		console.log(searchStr);
		let response = await http.get(`/getAllCheques?${searchStr}`);
		let { data } = response;
		console.log(data);
		this.setState({ data: data });
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.componentDidMount();
	}
	makeSearchString = (queryParams) => {
		let { page, bank = "", amount = "" } = queryParams;
		let searchStr = "";
		searchStr = this.addToQuery("page", page, searchStr);
		searchStr = this.addToQuery("bank", bank, searchStr);
		searchStr = this.addToQuery("amount", amount, searchStr);
		return searchStr;
	};
	addToQuery = (name, value, str) =>
		value
			? name === "page"
				? (str = str + `${name}=${value}`)
				: (str = str + `&${name}=${value}`)
			: str;
	handleChangeChoice = (choice) => {
		let searchStr = this.makeSearchString(choice);
		this.props.history.push(`/allCheques?${searchStr}`);
	};
	handlePageChange = (val) => {
		let queryParams = queryString.parse(this.props.location.search);
		let newPage = +queryParams.page + val;
		queryParams.page = newPage;
		let searchStr = this.makeSearchString(queryParams);
		console.log(searchStr);
		this.props.history.push(`/allCheques?${searchStr}`);
	};
	render() {
		let { data } = this.state;
		let { items, page, totalItems, totalNum } = data;
		let queryParams = queryString.parse(this.props.location.search);
		let end = page * 5;
		let start = end - 4;
		if (end > totalNum) {
			end = totalNum;
		}
		return (
			<div className='container mt-5'>
				<h2>All Cheque Transactions</h2>
				<div className='row'>
					<div className='col-3 ml-3'>
						<LeftPanel
							choice={queryParams}
							onOptionChange={this.handleChangeChoice}
						/>
					</div>
					<div className='col-8 '>
						<h5>
							{start} - {end} of {totalNum}
						</h5>
						<div className='row pt-4 pb-4 font-weight-bold  border-top border-bottom'>
							<div className='col-2'>Name</div>
							<div className='col-3'>Cheque Number</div>
							<div className='col-3'>Bank Name</div>
							<div className='col-2'>Branch</div>
							<div className='col-2'>Amount</div>
						</div>
						{items ? (
							<React.Fragment>
								{" "}
								{items.map((ele, index) => {
									return (
										<div
											className={
												index % 2 === 0
													? "row bg-light pt-4 pb-4 font-weight-bold border-top border-bottom"
													: "row pt-4 pb-4 font-weight-bold"
											}
										>
											<div className='col-2'>{ele.name}</div>
											<div className='col-3'>{ele.chequeNumber}</div>
											<div className='col-3'>{ele.bankName}</div>
											<div className='col-2'>{ele.branch}</div>
											<div className='col-2'>{ele.amount}</div>
										</div>
									);
								})}
								{page !== 1 ? (
									<button
										className='btn btn-secondary mt-3'
										onClick={() => this.handlePageChange(-1)}
									>
										Prev
									</button>
								) : (
									""
								)}
								{end === totalNum ? (
									""
								) : (
									<button
										className='btn btn-secondary float-right mt-3'
										onClick={() => this.handlePageChange(1)}
									>
										Next
									</button>
								)}
							</React.Fragment>
						) : (
							""
						)}
					</div>
				</div>
			</div>
		);
	}
}
export default AllCheques;
