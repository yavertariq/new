import React, { Component } from "react";
import http from "../../services/httpService";
import queryString from "query-string";

class AllCustomers extends Component {
	state = {
		data: {},
	};
	async componentDidMount() {
		let queryParams = queryString.parse(this.props.location.search);
		let { page } = queryParams;
		let response = await http.get(`/getCustomers?page=${page}`);
		let { data } = response;
		this.setState({ data: data });
	}
	async componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.componentDidMount();
	}
	handlePage = (val) => {
		let queryParams = queryString.parse(this.props.location.search);
		let newPage = +queryParams.page + val;
		this.props.history.push(`/allCustomers?page=${newPage}`);
	};
	render() {
		let { data } = this.state;
		let { page, items, totalNum, totalItems } = data;
		let end = page * 5;
		let start = end - 4;
		if (end > totalNum) {
			end = totalNum;
		}
		console.log(end);
		console.log(totalNum);
		return (
			<div className='container mt-5'>
				<h2>All Customers</h2>
				<h5>
					{start} - {end} of {totalNum}
				</h5>
				<div className='row p-3 font-weight-bold border-top border-bottom'>
					<div className='col-2'>Name</div>
					<div className='col-2'>State</div>
					<div className='col-2'>City</div>
					<div className='col-2'>PAN</div>
					<div className='col-2'>DOB</div>
				</div>
				{page
					? items.map((ele, index) => {
							return (
								<div
									className={
										index % 2 === 0
											? "row bg-light p-3 border-top border-bottom"
											: "row p-3"
									}
								>
									<div className='col-2'>{ele.name}</div>
									<div className='col-2'>{ele.state ? ele.state : ""}</div>
									<div className='col-2'>{ele.city ? ele.city : ""}</div>
									<div className='col-2'>{ele.PAN ? ele.PAN : ""}</div>
									<div className='col-2'>{ele.dob ? ele.dob : ""}</div>
								</div>
							);
					  })
					: ""}
				{page > 1 ? (
					<button
						className='btn btn-secondary mt-3'
						onClick={() => this.handlePage(-1)}
					>
						Prev
					</button>
				) : (
					""
				)}
				{end === totalNum ? (
					""
				) : (
					<button
						className='btn btn-secondary float-right mt-3'
						onClick={() => this.handlePage(1)}
					>
						Next
					</button>
				)}
			</div>
		);
	}
}
export default AllCustomers;
