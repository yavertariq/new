import React, { Component } from "react";
import http from "../../services/httpService";

class LeftPanel extends Component {
	state = {
		choice: this.props.choice,
		banks: [],
	};
	async componentDidMount() {
		let response = await http.get(`/getBanks`);
		let { data } = response;
		this.setState({ banks: data });
	}
	makeRadios = (arr, name, sel, label) => {
		return (
			<React.Fragment>
				<div className='border p-2 bg-light'>
					<h5 className='ml-2'>{label}</h5>
				</div>
				{arr.map((ele) => {
					return (
						<div className='form-check border p-3'>
							<input
								className='form-check-input ml-2'
								type='radio'
								name={name}
								value={ele}
								checked={sel === ele ? true : false}
								onChange={this.handleChange}
							></input>
							<label className='form-check-label font-weight-bold ml-4'>
								{ele}
							</label>
						</div>
					);
				})}
			</React.Fragment>
		);
	};
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.choice[input.name] = input.value;
		this.setState(s1);
		this.props.onOptionChange(s1.choice);
	};
	render() {
		let { choice, banks } = this.state;
		let amounts = ["<10000", ">=10000"];
		let { bank = "", amount = "" } = choice;
		return (
			<React.Fragment>
				{this.makeRadios(banks, "bank", bank, "Bank")}
				<br></br>
				{this.makeRadios(amounts, "amount", amount, "Amount")}
			</React.Fragment>
		);
	}
}
export default LeftPanel;
