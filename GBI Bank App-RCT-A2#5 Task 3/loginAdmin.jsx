import React, { Component } from "react";
import bank from "./bank.jpg";

class LoginAdmin extends Component {
	render() {
		return (
			<div className='container text-center'>
				<h2 style={{ color: "#C70039", margin: "40px 0" }}>
					Welcome to GBI Bank
				</h2>
				<img src={bank}></img>
			</div>
		);
	}
}
export default LoginAdmin;
