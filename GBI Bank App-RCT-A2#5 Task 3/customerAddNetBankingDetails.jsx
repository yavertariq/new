import React, { Component } from "react";
import http from "../../services/httpService";
import auth from "../../services/authService";

class AddNetBankingDetails extends Component {
	state = {
		data: {},
		netBanking: {},
		errors: { payeeName: "pop", amount: "pop" },
	};
	async componentDidMount() {
		let user = auth.getUser();
		let response = await http.get(`/getPayees/${user.name}`);
		let { data } = response;
		console.log(data);
		let s1 = { ...this.state };
		s1.data = data;
		s1.netBanking.name = user.name;
		s1.netBanking.bankName = "GBI";
		this.setState(s1);
	}
	async postData(url, obj) {
		let response = await http.post(url, obj);
		this.props.history.push("/");
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.netBanking[input.name] = input.value;
		let error = this.validateInput(input.value, input.name);
		s1.errors[input.name] = error;
		this.setState(s1);
	};
	validateInput = (value, name) => {
		let error = "";
		if (name === "amount") {
			error = value
				? value === "0"
					? "Amount must be greater than 0"
					: ""
				: "Cannot be Empty";
		}
		return error;
	};
	checkPayee = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		let error = "";
		error = s1.netBanking["payeeName"] ? "" : "Payee is required";

		s1.errors["payeeName"] = error;

		this.setState(s1);
	};
	makeDD = (arr, sel, label, name) => {
		return (
			<select
				className='form-control'
				value={sel}
				name={name}
				onChange={this.handleChange}
				onBlur={this.checkPayee}
			>
				<option disabled selected value=''>
					Select {label}
				</option>
				{arr.length > 0
					? arr.map((ele) => {
							return <option>{ele.payeeName}</option>;
					  })
					: ""}
			</select>
		);
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		let s1 = { ...this.state };
		if (this.validateErrors(s1.errors)) {
			alert("details Successfully Added");
			this.postData("/postNet", s1.netBanking);
		} else {
			alert("Please fill in all the details");
		}
	};
	render() {
		let { data = [], netBanking, errors } = this.state;
		let { payeeName = "", amount = "", comment = "" } = netBanking;
		return (
			<div className='container mt-4'>
				<h3>Net Banking Details</h3>
				<div className='form-group mt-4'>
					<label style={{ fontSize: "19px" }}>
						Payee Name<span className='text-danger'>*</span>
					</label>

					{this.makeDD(data, payeeName, "Payee", "payeeName")}

					{errors.payeeName && errors.payeeName !== "pop" && (
						<span
							className='form-control'
							style={{ background: "pink", color: "#a5002f" }}
						>
							{errors.payeeName}
						</span>
					)}
				</div>
				<div className='form-group'>
					<label style={{ fontSize: "19px" }}>
						Amount<span className='text-danger'>*</span>
					</label>
					<input
						className='form-control'
						type='number'
						name='amount'
						value={amount}
						placeholder=' Enter Amount'
						onChange={this.handleChange}
					></input>
					{errors.amount && errors.amount !== "pop" && (
						<span
							className='form-control'
							style={{ background: "pink", color: "#a5002f" }}
						>
							{errors.amount}
						</span>
					)}
				</div>
				<hr></hr>
				<div className='form-group'>
					<label style={{ fontSize: "19px" }}>Comment</label>
					<input
						className='form-control'
						name='comment'
						value={comment}
						placeholder=' Enter comment'
						onChange={this.handleChange}
					></input>
				</div>
				<button className='btn btn-primary' onClick={this.handleSubmit}>
					Add Transaction
				</button>
			</div>
		);
	}
}
export default AddNetBankingDetails;
