import React, { Component } from "react";
import http from "../../services/httpService";

class AddCustomer extends Component {
	state = {
		user: { name: "", password: "" },
		passCheck: "",
		error: { name: "pop", password: "pop", passCheck: "pop" },
	};
	async postData(url, obj) {
		let response = await http.post(url, obj);
		console.log(response);
		this.props.history.push("/admin");
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (input.name === "passCheck") {
			s1[input.name] = input.value;
		} else s1.user[input.name] = input.value;
		let error = this.validateInput(s1, input.name);
		s1.error[input.name] = error;
		this.setState(s1);
	};
	validateInput = (s1, nameCheck) => {
		let { user, passCheck } = s1;
		let { name, password } = user;
		let error = "";
		if (nameCheck === "name") {
			error = this.checkName(name);
		} else if (nameCheck === "password") {
			error = this.checkPassword(password);
		} else {
			error = password === passCheck ? "" : "Password does not match";
		}
		return error;
	};
	checkName = (str) => (str ? "" : "Name cannot be Empty");
	checkPassword = (str) =>
		str
			? str.length > 6
				? ""
				: "Password cannot be blank.Minimum length should be 7."
			: "Password cannot be blank.Minimum length should be 7.";
	checkErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		if (this.checkErrors(s1.error)) {
			alert("Customer successfully added");
			this.postData(`/register`, s1.user);
		} else {
			alert("Please check the details entered");
		}
	};
	render() {
		let { user, passCheck, error } = this.state;
		let { name, password } = user;
		return (
			<div className='container mt-5'>
				<h2>New Customer</h2>
				<div className='form-group'>
					<h5>Name</h5>
					<input
						className='form-control'
						name='name'
						placeholder=' Enter customer name'
						value={name}
						onChange={this.handleChange}
					></input>
				</div>
				<div className='form-group'>
					<h5>Password</h5>
					<input
						className='form-control'
						type='password'
						name='password'
						value={password}
						onChange={this.handleChange}
					></input>
					{error && error.password !== "pop" && (
						<span className='text-danger'>{error.password}</span>
					)}
				</div>
				<div className='form-group'>
					<h5>Confirm Password</h5>
					<input
						className='form-control'
						type='password'
						name='passCheck'
						value={passCheck}
						onChange={this.handleChange}
					></input>
				</div>
				<button className='btn btn-primary' onClick={this.handleSubmit}>
					Create
				</button>
			</div>
		);
	}
}
export default AddCustomer;
