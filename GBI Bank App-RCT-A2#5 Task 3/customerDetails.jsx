import React, { Component } from "react";
import http from "../../services/httpService";
import auth from "../../services/authService";

class CustomerDetails extends Component {
	state = {
		user: {},
		date: ["", "", ""],
		statesCities: this.props.statesCities,
		edit: false,
		errors: {
			gender: "pop",
			day: "pop",
			month: "pop",
			year: "pop",
			PAN: "pop",
			state: "pop",
			city: "pop",
		},
	};
	async componentDidMount() {
		let user = auth.getUser();
		let response = await http.get(`/getCustomer/${user.name}`);
		let { data } = response;
		console.log(data);
		let date = [];
		let s1 = { ...this.state };
		if (data) {
			date = data.dob.split("-");
			s1.user = data;
			s1.date = date;
		} else {
			s1.user["name"] = user.name;
			s1.edit = true;
		}
		this.setState(s1);
	}
	async postData(url, obj) {
		let response = await http.post(url, obj);
		this.props.history.push("/");
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (s1.edit) {
			if (
				input.name === "day" ||
				input.name === "month" ||
				input.name === "year"
			) {
				if (input.name === "day") {
					s1.date[0] = input.value;
				} else if (input.name === "month") {
					s1.date[1] = input.value;
				} else {
					s1.date[2] = input.value;
				}
				s1.user["dob"] = s1.date.join("-");
			} else s1.user[input.name] = input.value;
		}
		s1.errors[input.name] = this.validateInput(input.value, input.name);
		this.setState(s1);
	};
	validateInput = (val, name) => {
		let error = "";

		if (name === "gender" || name === "PAN") {
			error = this.checkInput(val);
		}
		return error;
	};
	checkInput = (val) => (val ? "" : "Mandatory Field");
	makeDD = (arr, sel, name) => {
		let arr2 = [];
		console.log(arr);
		if (name === "city") {
			arr2 = arr.reduce((acc, curr) => {
				if (curr.stateName === this.state.user["state"]) acc = curr.cityArr;
				return acc;
			}, []);
			sel = sel.trim();
		}
		console.log(sel);
		return (
			<select
				className='form-control'
				value={sel}
				name={name}
				onChange={this.handleChange}
				onBlur={() => this.checkDD(name)}
			>
				<option selected disabled value=''>
					Select {name}
				</option>
				{name === "state"
					? arr.map((ele) => {
							return <option>{ele.stateName}</option>;
					  })
					: name === "city"
					? arr2.map((ele) => {
							return <option>{ele}</option>;
					  })
					: arr.map((ele) => {
							return <option>{ele}</option>;
					  })}
			</select>
		);
	};
	checkDD = (name) => {
		let s1 = { ...this.state };
		let error = "";
		if (name !== "day" && name !== "month" && name !== "year")
			error = s1.user[name] ? "" : `${name} is required`;
		else if (name === "day") {
			error = s1.date[0] ? "" : "Day is required";
		} else if (name === "month") {
			error = s1.date[1] ? "" : "Month is required";
		} else {
			error = s1.date[2] ? "" : "Year is required";
		}
		s1.errors[name] = error;

		this.setState(s1);
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		let user = auth.getUser();
		e.preventDefault();
		let s1 = { ...this.state };
		if (this.validateErrors(s1.errors)) {
			alert(`${user.name} details Added Successfully`);
			this.postData("/customerDetails", s1.user);
		} else {
			alert("Enter the mandatory details");
		}
	};
	render() {
		let { user, statesCities, date, edit, errors } = this.state;
		let {
			name = "",
			gender = "",
			addressLine1 = "",
			addressLine2 = "",
			state = "",
			city = "",
			dob = "",
			PAN = "",
		} = user;
		let years = [];
		for (let i = 1940; i <= 2021; i++) {
			years.push(i);
		}
		let months = [
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December",
		];
		let months31 = [
			"January",
			"March",
			"May",
			"July",
			"August",
			"October",
			"December",
		];

		let months30 = ["April", "June", "September", "November"];
		let daysNum =
			months31.indexOf(date[1]) >= 0
				? 31
				: months30.indexOf(date[1]) >= 0
				? 30
				: 0;
		if (daysNum === 0) {
			if (date[2] % 4 === 0) {
				daysNum = 29;
			} else daysNum = 28;
		}
		let days = [];
		for (let i = 1; i <= daysNum; i++) {
			days.push(i);
		}
		return (
			<div className='container mt-4'>
				<h3>Customer Details</h3>

				{name ? (
					<React.Fragment>
						<label
							className='form-check-label font-weight-bold mt-5'
							style={{ fontSize: "19px", marginRight: "100px" }}
						>
							Gender<span className='text-danger'>*</span>
						</label>
						<div className='form-check form-check-inline'>
							<input
								className='form-check-input'
								type='radio'
								name='gender'
								value='Male'
								checked={gender === "Male" ? true : false}
								onChange={this.handleChange}
							></input>
							<label
								className='form-check-label'
								style={{ marginRight: "180px" }}
							>
								Male
							</label>
							<input
								className='form-check-input'
								type='radio'
								name='gender'
								value='Female'
								checked={gender === "Female" ? true : false}
								onChange={this.handleChange}
							></input>
							<label className='form-check-label'>Female</label>
						</div>
						{errors.gender &&
							errors.gender !== "pop" &&
							errors.gender !== "Mandatory Field" && (
								<span
									className='form-control'
									style={{ background: "pink", color: "#a5002f" }}
								>
									{errors.gender}
								</span>
							)}
						<hr></hr>
						<div className='form-group'>
							<label className='font-weight-bold' style={{ fontSize: "19px" }}>
								Date of Birth<span className='text-danger'>*</span>
							</label>
							<div className='row'>
								<div className='col-4'>
									{this.makeDD(years, date[2], "year")}
								</div>
								<div className='col-4'>
									{this.makeDD(months, date[1], "month")}
								</div>
								<div className='col-4'>{this.makeDD(days, date[0], "day")}</div>
								<div className='col-4'>
									{errors.year &&
										errors.year !== "pop" &&
										errors.year !== "Mandatory Field" && (
											<span
												className='form-control'
												style={{ background: "pink", color: "#a5002f" }}
											>
												{errors.year}
											</span>
										)}
								</div>
								<div className='col-4'>
									{errors.month &&
										errors.month !== "pop" &&
										errors.month !== "Mandatory Field" && (
											<span
												className='form-control'
												style={{ background: "pink", color: "#a5002f" }}
											>
												{errors.month}
											</span>
										)}
								</div>
								<div className='col-4'>
									{errors.day &&
										errors.day !== "pop" &&
										errors.day !== "Mandatory Field" && (
											<span
												className='form-control'
												style={{ background: "pink", color: "#a5002f" }}
											>
												{errors.day}
											</span>
										)}
								</div>
							</div>
						</div>
						<div className='form-group'>
							<label className='font-weight-bold' style={{ fontSize: "19px" }}>
								PAN<span className='text-danger'>*</span>
							</label>
							<input
								className='form-control'
								name='PAN'
								value={PAN}
								onChange={this.handleChange}
							></input>
							{errors.PAN &&
								errors.PAN !== "pop" &&
								errors.PAN !== "Mandatory Field" && (
									<span
										className='form-control'
										style={{ background: "pink", color: "#a5002f" }}
									>
										{errors.PAN}
									</span>
								)}
						</div>
						<div className='form-group'>
							<label className='font-weight-bold' style={{ fontSize: "19px" }}>
								Address
							</label>
							<div className='row'>
								<div className='col-4'>
									<input
										className='form-control'
										name='addressLine1'
										value={addressLine1}
										placeholder=' Line 1'
										onChange={this.handleChange}
									></input>
								</div>
								<div className='col-4'>
									<input
										className='form-control'
										name='addressLine2'
										value={addressLine2}
										placeholder=' Line 2'
										onChange={this.handleChange}
									></input>
								</div>
							</div>
						</div>
						<div className='form-group row'>
							<div className='col-6'>
								<label
									className='font-weight-bold'
									style={{ fontSize: "19px" }}
								>
									State<span className='text-danger'>*</span>
								</label>
								{this.makeDD(statesCities, state, "state")}
							</div>
							<div className='col-6'>
								<label
									className='font-weight-bold'
									style={{ fontSize: "19px" }}
								>
									City<span className='text-danger'>*</span>
								</label>
								{this.makeDD(statesCities, city, "city")}
							</div>
							<div className='col-6'>
								{errors.state &&
									errors.state !== "pop" &&
									errors.state !== "Mandatory Field" && (
										<span
											className='form-control'
											style={{ background: "pink", color: "#a5002f" }}
										>
											{errors.state}
										</span>
									)}
							</div>
							<div className='col-6'>
								{errors.city &&
									errors.city !== "pop" &&
									errors.city !== "Mandatory Field" && (
										<span
											className='form-control'
											style={{ background: "pink", color: "#a5002f" }}
										>
											{errors.city}
										</span>
									)}
							</div>
						</div>
						{edit ? (
							<button className='btn btn-primary' onClick={this.handleSubmit}>
								Add Details
							</button>
						) : (
							""
						)}
					</React.Fragment>
				) : (
					""
				)}
			</div>
		);
	}
}
export default CustomerDetails;
