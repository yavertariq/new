import React, { Component } from "react";
import http from "../../services/httpService";
import auth from "../../services/authService";
import queryString from "query-string";

class ViewCheque extends Component {
	state = { data: {} };
	async componentDidMount() {
		let user = auth.getUser();
		let queryParams = queryString.parse(this.props.location.search);
		let { page } = queryParams;
		let response = await http.get(`/getChequeByName/${user.name}?page=${page}`);
		let { data } = response;
		this.setState({ data: data });
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.componentDidMount();
	}
	handlePageChange = (val) => {
		let queryParams = queryString.parse(this.props.location.search);
		let newPage = +queryParams.page + val;
		queryParams.page = newPage;
		this.props.history.push(`/viewCheque?page=${newPage}`);
	};
	render() {
		let { data } = this.state;
		let { items = [], page, totalNum, totalItems } = data;
		let end = page * 5;
		let start = end - 4;
		if (end > totalNum) {
			end = totalNum;
		}
		return (
			<div className='container mt-5'>
				<h2>All Cheque Details</h2>
				{items.length > 0 ? (
					<React.Fragment>
						<h5>
							{start} - {end} of {totalNum}
						</h5>
						<div
							className={
								"row text-center pt-4 pb-4 border-top border-bottom font-weight-bold"
							}
						>
							<div className='col-4'>Cheque Number</div>
							<div className='col-3'>Bank Name</div>
							<div className='col-3'>Branch</div>
							<div className='col-2'>Amount</div>
						</div>
						{items ? (
							<React.Fragment>
								{items.map((ele, index) => {
									return (
										<div
											className={
												"row text-center pt-4 pb-4 " +
												(index % 2 === 0
													? "bg-light border-top border-bottom"
													: "")
											}
										>
											<div className='col-4'>{ele.chequeNumber}</div>
											<div className='col-3'>{ele.bankName}</div>
											<div className='col-3'>{ele.branch}</div>
											<div className='col-2'>{ele.amount}</div>
										</div>
									);
								})}
								{page > 1 ? (
									<button
										className='btn btn-secondary mt-3'
										onClick={() => this.handlePageChange(-1)}
									>
										Prev
									</button>
								) : (
									""
								)}
								{end === totalNum ? (
									""
								) : (
									<button
										className='btn btn-secondary mt-3 float-right'
										onClick={() => this.handlePageChange(1)}
									>
										Next
									</button>
								)}
							</React.Fragment>
						) : (
							""
						)}
					</React.Fragment>
				) : end === 0 ? (
					<h4 style={{ color: "#df3666" }}>No Transactions to show</h4>
				) : (
					""
				)}
			</div>
		);
	}
}
export default ViewCheque;
