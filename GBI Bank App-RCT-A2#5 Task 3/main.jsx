import React, { Component } from "react";
import { Link, Route, Switch, Redirect } from "react-router-dom";
import Login from "./login";
import auth from "../../services/authService";
import LoginAdmin from "./loginAdmin";
import Logout from "./logout";
import AllCustomers from "./allCustomers";
import AddCustomer from "./addCustomer";
import AllCheques from "./allCheques";
import AllNetBanking from "./allNetBanking";
import LoginCustomer from "./loginCustomer";
import ViewCheque from "./customerViewCheque";
import ViewNetBanking from "./customerNetBanking";
import CustomerDetails from "./customerDetails";
import http from "../../services/httpService";
import NomineeDetails from "./customerNominee";
import DepositCheque from "./depositCheque";
import AddNetBankingDetails from "./customerAddNetBankingDetails";
import AddPayee from "./customerAddPayee";
import NotAllowed from "./notAllowed";

class MainComponent extends Component {
	state = { statesCities: [] };
	async componentDidMount() {
		let response = await http.get(`/statecity`);
		let { data } = response;
		this.setState({ statesCities: data });
	}
	render() {
		let { statesCities } = this.state;
		let user = auth.getUser();
		return (
			<React.Fragment>
				<nav className='navbar navbar-expand-sm navbar-light bg-warning'>
					<Link className='navbar-brand' to='/' style={{ fontSize: "25px" }}>
						Home
					</Link>
					<ul className='navbar-nav'>
						{!user && (
							<li className='nav-item'>
								<Link className='nav-link'>Login</Link>
							</li>
						)}
						{user && user.role === "manager" && (
							<li className='nav-item dropdown'>
								<Link
									className='nav-link dropdown-toggle'
									data-toggle='dropdown'
								>
									Customers
								</Link>
								<div className='dropdown-menu'>
									<Link className='dropdown-item' to='/addCustomer'>
										Add Customer
									</Link>
									<Link className='dropdown-item' to='/allCustomers?page=1'>
										View All Customers
									</Link>
								</div>
							</li>
						)}
						{user && user.role === "manager" && (
							<li className='nav-item dropdown'>
								<Link
									className='nav-link dropdown-toggle'
									data-toggle='dropdown'
								>
									Transactions
								</Link>
								<div className='dropdown-menu'>
									<Link className='dropdown-item' to='/allCheques?page=1'>
										Cheques
									</Link>
									<Link className='dropdown-item' to='allNet?page=1'>
										All Transactions
									</Link>
								</div>
							</li>
						)}
						{user && user.role === "customer" && (
							<li className='nav-item dropdown'>
								<Link
									className='nav-link dropdown-toggle'
									data-toggle='dropdown'
								>
									View
								</Link>
								<div className='dropdown-menu'>
									<Link className='dropdown-item' to='/viewCheque?page=1'>
										Cheque
									</Link>
									<Link className='dropdown-item' to='/viewNet?page=1'>
										Net Banking
									</Link>
								</div>
							</li>
						)}
						{user && user.role === "customer" && (
							<li className='nav-item dropdown'>
								<Link
									className='nav-link dropdown-toggle'
									data-toggle='dropdown'
								>
									Details
								</Link>
								<div className='dropdown-menu'>
									<Link className='dropdown-item' to='/customerDetails'>
										Customer
									</Link>
									<Link className='dropdown-item' to='/nomineeDetails'>
										Nominee
									</Link>
								</div>
							</li>
						)}
						{user && user.role === "customer" && (
							<li className='nav-item dropdown'>
								<Link
									className='nav-link dropdown-toggle'
									data-toggle='dropdown'
								>
									Transaction
								</Link>
								<div className='dropdown-menu'>
									<Link className='dropdown-item' to='/addPayee'>
										Add Payee
									</Link>
									<Link className='dropdown-item' to='/cheque'>
										Cheque
									</Link>
									<Link className='dropdown-item' to='/netBanking'>
										Net Banking
									</Link>
								</div>
							</li>
						)}
					</ul>
					{user && (
						<ul className='navbar-nav ml-auto'>
							<li className='nav-item mr-2'>
								<a className='nav-link'>Welcome {user.name}</a>
							</li>
							<li className='nav-item'>
								<Link className='nav-link' to='/logout'>
									Logout
								</Link>
							</li>
						</ul>
					)}
				</nav>
				<Switch>
					<Route path='/notAllowed' component={NotAllowed} />
					<Route
						path='/addPayee'
						render={(props) =>
							user ? (
								user.role === "customer" ? (
									<AddPayee {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/netBanking'
						render={(props) =>
							user ? (
								user.role === "customer" ? (
									<AddNetBankingDetails {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/cheque'
						render={(props) =>
							user ? (
								user.role === "customer" ? (
									<DepositCheque {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/nomineeDetails'
						render={(props) =>
							user ? (
								user.role === "customer" ? (
									<NomineeDetails {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/customerDetails'
						render={(props) =>
							user ? (
								user.role === "customer" ? (
									<CustomerDetails {...props} statesCities={statesCities} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/viewNet'
						render={(props) =>
							user ? (
								user.role === "customer" ? (
									<ViewNetBanking {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/viewCheque'
						render={(props) =>
							user ? (
								user.role === "customer" ? (
									<ViewCheque {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/customer'
						render={(props) =>
							user ? (
								user.role === "customer" ? (
									<LoginCustomer {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/allNet'
						render={(props) =>
							user ? (
								user.role === "manager" ? (
									<AllNetBanking {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/allCheques'
						render={(props) =>
							user ? (
								user.role === "manager" ? (
									<AllCheques {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/addCustomer'
						render={(props) =>
							user ? (
								user.role === "manager" ? (
									<AddCustomer {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/allCustomers'
						render={(props) =>
							user ? (
								user.role === "manager" ? (
									<AllCustomers {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/admin'
						render={(props) =>
							user ? (
								user.role === "manager" ? (
									<LoginAdmin {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/logout'
						render={(props) =>
							user ? <Logout {...props} /> : <Redirect to='/login' />
						}
					/>
					<Route
						path='/login'
						render={(props) =>
							!user ? <Login {...props} /> : <Redirect to='/notAllowed' />
						}
					/>
					<Redirect
						from='/'
						to={
							user
								? user.role === "manager"
									? "/admin"
									: "/customer"
								: "/login"
						}
					/>
				</Switch>
			</React.Fragment>
		);
	}
}
export default MainComponent;
