import React, { Component } from "react";

class NotAllowed extends Component {
	render() {
		return (
			<div className='container'>
				<h2>This functionality is not allowed</h2>
			</div>
		);
	}
}
export default NotAllowed;
