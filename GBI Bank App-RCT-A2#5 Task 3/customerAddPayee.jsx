import React, { Component } from "react";
import auth from "../../services/authService";
import http from "../../services/httpService";

class AddPayee extends Component {
	state = {
		payee: {},
		errors: {
			payeeName: "pop",
			accNumber: "pop",
			IFSC: "pop",
			bankName: "",
		},
		other: false,
		banks: [],
	};
	async componentDidMount() {
		let user = auth.getUser();
		let response = await http.get(`/getBanks`);
		let { data } = response;
		let s1 = { ...this.state };
		s1.payee["name"] = user.name;
		s1.payee["IFSC"] = "";
		s1.payee["bankName"] = "GBI";
		s1.banks = data;
		this.setState(s1);
	}
	async postData(url, obj) {
		let response = await http.post(url, obj);
		this.props.history.push("/");
	}
	makeDD = (arr, sel, label, name) => {
		return (
			<select
				className='form-control'
				value={sel}
				name={name}
				onChange={this.handleChange}
			>
				<option>Select {label}</option>
				{arr.map((ele) => {
					return (
						<option value={name === "month" ? ele.substring(0, 3) : ele}>
							{ele}
						</option>
					);
				})}
			</select>
		);
	};
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (input.name === "bankName") {
			if (input.value === "GBI") {
				s1.payee[input.name] = input.value;
				s1.other = false;
			} else {
				s1.other = true;
				if (input.value === "others") s1.payee[input.name] = "";
				else s1.payee[input.name] = input.value;
			}
		} else s1.payee[input.name] = input.value;
		let error = this.validateInput(s1.payee[input.name], input.name);
		s1.errors[input.name] = error;
		this.setState(s1);
	};
	validateInput = (val, name) => {
		let error = "";
		if (name === "accNumber") {
			error = val
				? val.length > 8
					? ""
					: "Account number should be atleast 9 characters"
				: "Cannot be Empty";
		} else {
			error = val ? "" : "Cannot be Empty";
		}
		return error;
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		if (this.validateErrors(s1.errors)) {
			alert(`Payee added to your list::${s1.payee["payeeName"]}`);
			this.postData(`/addPayee`, s1.payee);
		} else {
			alert("Please fill the mandatory details");
		}
	};
	render() {
		let { payee, errors, other, banks } = this.state;
		let { payeeName = "", accNumber = "", bankName, IFSC } = payee;
		return (
			<div className='container mt-4'>
				<h3>Add Payee</h3>
				<div className='form-group mt-3'>
					<label style={{ fontSize: "19px" }}>
						Payee Name<span className='text-danger'>*</span>
					</label>
					<input
						className='form-control'
						name='payeeName'
						value={payeeName}
						onChange={this.handleChange}
						placeholder=' Enter payee name'
					></input>
					{errors.payeeName && errors.payeeName !== "pop" && (
						<span
							className='form-control'
							style={{ background: "pink", color: "#a5002f" }}
						>
							{errors.payeeName}
						</span>
					)}
				</div>
				<div className='form-group mt-3'>
					<label style={{ fontSize: "19px" }}>
						Account Number<span className='text-danger'>*</span>
					</label>
					<input
						className='form-control'
						name='accNumber'
						value={accNumber}
						onChange={this.handleChange}
						placeholder=' Enter payee account number'
					></input>
					{errors.accNumber &&
						errors.accNumber !== "pop" &&
						errors.accNumber.length === 15 && (
							<span
								className='form-control'
								style={{ background: "pink", color: "#a5002f" }}
							>
								{errors.accNumber}
							</span>
						)}
				</div>
				<div className='form-check'>
					<input
						className='form-check-input'
						name='bankName'
						type='radio'
						value='GBI'
						checked={bankName === "GBI" ? true : false}
						onChange={this.handleChange}
					></input>
					<label className='form-check-label'>Same Bank</label>
				</div>
				<div className='form-check'>
					<input
						className='form-check-input'
						name='bankName'
						value='others'
						type='radio'
						onChange={this.handleChange}
					></input>
					<label className='form-check-label'>Other Bank</label>
				</div>
				{other ? (
					<React.Fragment>
						<div className='form-group'>
							{this.makeDD(banks, bankName, "Bank", "bankName")}
						</div>
						<div className='form-group'>
							<label style={{ fontSize: "19px" }}>
								IFSC Code<span className='text-danger'>*</span>
							</label>
							<input
								className='form-control'
								name='IFSC'
								value={IFSC}
								onChange={this.handleChange}
								placeholder=' Enter IFSC Code'
							></input>
							{errors.IFSC && errors.IFSC !== "pop" && (
								<span
									className='form-control'
									style={{ background: "pink", color: "#a5002f" }}
								>
									{errors.IFSC}
								</span>
							)}
						</div>
					</React.Fragment>
				) : (
					""
				)}
				<button className='btn btn-primary mt-4' onClick={this.handleSubmit}>
					Add Payee
				</button>
			</div>
		);
	}
}
export default AddPayee;
