import React, { Component } from "react";
import A8SectionAProducts from "./a8SectionAProducts";

class A8SectionAMain extends Component {
	state = {
		products: [
			{
				code: "PEP221",
				prod: "Pepsi",
				price: 12,
				instock: "Yes",
				category: "Beverages",
			},
			{
				code: "COK113",
				prod: "Coca Cola",
				price: 18,
				instock: "Yes",
				category: "Beverages",
			},
			{
				code: "MIR646",
				prod: "Mirinda",
				price: 15,
				instock: "No",
				category: "Beverages",
			},
			{
				code: "SLI874",
				prod: "Slice",
				price: 22,
				instock: "Yes",
				category: "Beverages",
			},
			{
				code: "MIN654",
				prod: "Minute Maid",
				price: 25,
				instock: "Yes",
				category: "Beverages",
			},
			{
				code: "APP652",
				prod: "Appy",
				price: 10,
				instock: "No",
				category: "Beverages",
			},
			{
				code: "FRO085",
				prod: "Frooti",
				price: 30,
				instock: "Yes",
				category: "Beverages",
			},
			{
				code: "REA546",
				prod: "Real",
				price: 24,
				instock: "No",
				category: "Beverages",
			},
			{
				code: "DM5461",
				prod: "Dairy Milk",
				price: 40,
				instock: "Yes",
				category: "Chocolates",
			},
			{
				code: "KK6546",
				prod: "Kitkat",
				price: 15,
				instock: "Yes",
				category: "Chocolates",
			},
			{
				code: "PER5436",
				prod: "Perk",
				price: 8,
				instock: "No",
				category: "Chocolates",
			},
			{
				code: "FST241",
				prod: "5 Star",
				price: 25,
				instock: "Yes",
				category: "Chocolates",
			},
			{
				code: "NUT553",
				prod: "Nutties",
				price: 18,
				instock: "Yes",
				category: "Chocolates",
			},
			{
				code: "GEM006",
				prod: "Gems",
				price: 8,
				instock: "No",
				category: "Chocolates",
			},
			{
				code: "GD2991",
				prod: "Good Day",
				price: 25,
				instock: "Yes",
				category: "Biscuits",
			},
			{
				code: "PAG542",
				prod: "Parle G",
				price: 5,
				instock: "Yes",
				category: "Biscuits",
			},
			{
				code: "MON119",
				prod: "Monaco",
				price: 7,
				instock: "No",
				category: "Biscuits",
			},
			{
				code: "BOU291",
				prod: "Bourbon",
				price: 22,
				instock: "Yes",
				category: "Biscuits",
			},
			{
				code: "MAR951",
				prod: "MarieGold",
				price: 15,
				instock: "Yes",
				category: "Biscuits",
			},
			{
				code: "ORE188",
				prod: "Oreo",
				price: 30,
				instock: "No",
				category: "Biscuits",
			},
		],
		currentBill: [],
		view: 0,
		close: -1,
	};
	handleAdd = (bill) => {
		let s1 = { ...this.state };
		s1.currentBill = bill;
		this.setState(s1);
	};
	handleCurrentBill = (val, index) => {
		let s1 = { ...this.state };
		s1.currentBill[index].quantity += val;
		s1.currentBill[index].value =
			s1.currentBill[index].price * s1.currentBill[index].quantity;
		if (s1.currentBill[index].quantity === 0) s1.currentBill.splice(index, 1);
		this.setState(s1);
	};
	handleRemove = (index) => {
		let s1 = { ...this.state };
		s1.currentBill.splice(index, 1);
		this.setState(s1);
	};
	handleChangeView = (val) => {
		let s1 = { ...this.state };
		s1.view = 1;
		this.setState(s1);
	};
	handleCloseBill = () => {
		let s1 = { ...this.state };
		alert("Closing the Current Bill");
		s1.currentBill = [];
		s1.close = 1;
		this.setState(s1);
	};
	changeClose = () => {
		let s1 = { ...this.state };
		s1.close = -1;
		this.setState(s1);
	};
	render() {
		let { products, currentBill, view, close } = this.state;
		return (
			<div className='container'>
				<nav className='navbar navbar-expand-sm navbar-dark bg-dark'>
					<a className='navbar-brand' href='#'>
						BillingSystem
					</a>
					<div className='' id='navbarSupportedContent'>
						<ul className='navbar-nav mr-auto'>
							<li className='nav-item'>
								<a
									className='nav-link'
									href='#'
									onClick={() => {
										this.handleChangeView(1);
									}}
								>
									New Bill
								</a>
							</li>
						</ul>
					</div>
				</nav>
				{view === 1 ? (
					<React.Fragment>
						<h2>Details of Current Bill</h2>
						Items: {currentBill.length}, Quantity:{" "}
						{currentBill.reduce((acc, curr) => {
							return acc + curr.quantity;
						}, 0)}
						, Amount:{" "}
						{currentBill.reduce((acc, curr) => {
							return acc + curr.value;
						}, 0)}
						<br></br>
						{currentBill.map((ele, index) => {
							return (
								<div className='row border'>
									<div className='col-6'>
										{ele.code +
											" " +
											ele.prod +
											" Price:" +
											ele.price +
											" Quantity:" +
											ele.quantity +
											" Value:" +
											ele.value}
									</div>
									<div className='col-6'>
										<button
											className='btn btn-success btn-sm mr-1'
											onClick={() => this.handleCurrentBill(1, index)}
										>
											+
										</button>
										<button
											className='btn btn-warning btn-sm mr-1'
											onClick={() => this.handleCurrentBill(-1, index)}
										>
											-
										</button>
										<button
											className='btn btn-danger btn-sm mr-1'
											onClick={() => this.handleRemove(index)}
										>
											x
										</button>
									</div>
								</div>
							);
						})}
						{currentBill.length > 0 ? (
							<button
								className='btn btn-primary'
								onClick={() => this.handleCloseBill()}
							>
								Close Bill
							</button>
						) : (
							""
						)}
						<A8SectionAProducts
							products={products}
							currentBill={currentBill}
							onAdd={this.handleAdd}
							close={close}
							onClose={this.changeClose}
						/>
					</React.Fragment>
				) : (
					""
				)}
			</div>
		);
	}
}
export default A8SectionAMain;
