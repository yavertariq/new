import React, { Component } from "react";

class A8SectionBPizza extends Component {
	state = {
		pizzas: this.props.pizzas,
		cart: this.props.cart,
		selSize: { value: "", id: "" },
		selCrust: { value: "", id: "" },
		errors: [],
	};
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1[input.name].value = input.value;
		s1[input.name].id = input.id;
		this.setState(s1);
	};
	handleAddToCart = (index) => {
		let s1 = { ...this.state };
		let ent = {};
		ent = s1.pizzas[index];
		ent.size = s1.selSize.value;
		ent.crust = s1.selCrust.value;
		ent.quantity = 1;
		let errors = this.validateAll(ent);
		if (this.validateErrors(errors)) {
			s1.selCrust = { id: "", value: "" };
			s1.selSize = { id: "", value: "" };
			s1.cart.push(ent);
			this.props.onAddToCart(s1.cart);
		} else {
			s1.errors = errors;
		}
		this.setState(s1);
	};
	validateAll = (p1) => {
		let errors = {};
		console.log(p1);
		errors.size = p1.size === "" ? "Choose the Size before adding to cart" : "";
		if (errors.size === "") {
			errors.crust =
				p1.crust === "" ? "Choose the Crust before adding to cart" : "";
		}
		return errors;
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleChangeQuantity = (index, val) => {
		let s1 = { ...this.state };
		s1.cart[index].quantity += val;
		if (s1.cart[index].quantity === 0) s1.cart.splice(index, 1);
		this.setState(s1);
		this.props.onAddToCart(s1.cart);
	};
	render() {
		let { pizzas, cart, selSize, selCrust, errors } = this.state;
		let { sizes, crusts } = this.props;
		if (pizzas.length != this.props.pizzas.length) {
			let s1 = { ...this.state };
			s1.pizzas = this.props.pizzas;
			this.setState(s1);
		}
		return (
			<div className='row border bg-light'>
				<div className='col-8'>
					<div className='row'>
						{pizzas.map((ele, index) => {
							let check;
							let ind = cart.findIndex((elem) => {
								return elem.id === ele.id;
							});
							if (ind >= 0) {
								check = true;
							} else {
								check = false;
							}
							return (
								<div className='col-6 text-center border'>
									<img src={ele.image} className='img-fluid m-1'></img>
									<h5>{ele.name}</h5>
									<br></br>
									{ele.desc}
									<br></br>
									<div className='row'>
										<div className='col-6'>
											<select
												className='form-control'
												name='selSize'
												id={"selSize" + index}
												value={
													check
														? cart[ind].size
														: selSize.id === "selSize" + index
														? selSize.value
														: ""
												}
												disabled={check}
												onChange={this.handleChange}
											>
												<option selected>Select Size</option>
												{sizes.map((ele) => {
													return <option>{ele}</option>;
												})}
											</select>
											{errors.size ? (
												<React.Fragment>
													{alert(errors.size)}
													{(errors.size = "")}
												</React.Fragment>
											) : (
												""
											)}
										</div>
										<div className='col-6'>
											<select
												className='form-control'
												name='selCrust'
												value={
													check
														? cart[ind].crust
														: selCrust.id === "selCrust" + index
														? selCrust.value
														: ""
												}
												id={"selCrust" + index}
												onChange={this.handleChange}
												disabled={check}
											>
												<option selected>Select Crust</option>
												{crusts.map((ele) => {
													return <option>{ele}</option>;
												})}
											</select>
											{errors.crust ? (
												<React.Fragment>
													{alert(errors.crust)}
													{(errors.crust = "")}
												</React.Fragment>
											) : (
												""
											)}
										</div>
									</div>

									{check ? (
										<React.Fragment>
											<button
												className='btn btn-danger'
												onClick={() => this.handleChangeQuantity(ind, -1)}
											>
												-
											</button>
											<button className='btn btn-secondary' disabled>
												{cart[ind].quantity}
											</button>
											<button
												className='btn btn-success'
												onClick={() => this.handleChangeQuantity(ind, 1)}
											>
												+
											</button>
										</React.Fragment>
									) : (
										<button
											className='btn btn-primary m-1'
											onClick={() => this.handleAddToCart(index)}
										>
											Add To Cart
										</button>
									)}
								</div>
							);
						})}
					</div>
				</div>
				<div className='col-4 text-center'>
					{cart.length === 0 ? (
						<h3>Cart is empty</h3>
					) : (
						<React.Fragment>
							<h3>Cart</h3>
							{cart.map((ele, index) => {
								return (
									<div className='row border'>
										<div className='col-4 text-center my-auto'>
											<img src={ele.image} className='img-fluid'></img>
										</div>
										<div className='col-8 text-left'>
											<h5 className='font-weight-bold'>{ele.name}</h5>
											{ele.desc}
											<br></br>
											{ele.type === "Pizza" ? (
												<h5>
													{ele.size}|{ele.crust}
												</h5>
											) : (
												""
											)}
											<button
												className='btn btn-danger'
												onClick={() => this.handleChangeQuantity(index, -1)}
											>
												-
											</button>
											<button className='btn btn-secondary' disabled>
												{ele.quantity}
											</button>
											<button
												className='btn btn-success'
												onClick={() => this.handleChangeQuantity(index, 1)}
											>
												+
											</button>
										</div>
									</div>
								);
							})}
						</React.Fragment>
					)}
				</div>
			</div>
		);
	}
}
export default A8SectionBPizza;
