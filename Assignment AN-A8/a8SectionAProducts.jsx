import React, { Component } from "react";

class A8SectionAProducts extends Component {
	state = {
		products: this.props.products,
		selectedFilter: { category: "", instock: "", price: "" },
		sortCol: "",
		currentBill: this.props.currentBill,
	};
	handleClose = () => {
		let s1 = { ...this.state };
		s1.selectedFilter = { category: "", instock: "", price: "" };
		s1.sortCol = "";
		s1.currentBill = [];
		this.setState(s1);
	};
	handleSort = (col) => {
		let s1 = { ...this.state };
		switch (col) {
			case 1:
				s1.products.sort((p1, p2) => {
					return p1.code.localeCompare(p2.code);
				});
				s1.sortCol = 1;
				break;
			case 2:
				s1.products.sort((p1, p2) => {
					return p1.prod.localeCompare(p2.prod);
				});
				s1.sortCol = 2;
				break;
			case 3:
				s1.products.sort((p1, p2) => {
					return p1.category.localeCompare(p2.category);
				});
				s1.sortCol = 3;
				break;
			case 4:
				s1.products.sort((p1, p2) => {
					return p1.price - p2.price;
				});
				s1.sortCol = 4;
				break;
			case 5:
				s1.products.sort((p1, p2) => {
					return p1.instock.localeCompare(p2.instock);
				});
				s1.sortCol = 5;
				break;
			default:
				break;
		}
		this.setState(s1);
	};
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.selectedFilter[input.name] = input.value;
		this.setState(s1);
	};
	handleAddToBill = (code) => {
		let s1 = { ...this.state };
		let index = s1.products.findIndex((ele) => {
			return ele.code === code;
		});
		let ent = { code: "", prod: "", price: 0, quantity: 0, value: 0 };
		ent.code = s1.products[index].code;
		ent.prod = s1.products[index].prod;
		ent.price = s1.products[index].price;

		let index2 = s1.currentBill.findIndex((ele) => {
			return ele.code === ent.code;
		});
		console.log(index2, s1.currentBill.length);
		if (index2 === -1 || s1.currentBill.length === 0) {
			ent.quantity = 1;
			ent.value = ent.quantity * ent.price;
			s1.currentBill.push(ent);
		} else {
			console.log(s1.currentBill[index2].quantity);
			s1.currentBill[index2].quantity += 1;
			s1.currentBill[index2].value =
				s1.currentBill[index2].quantity * s1.currentBill[index2].price;
		}
		this.setState(s1);
		this.props.onAdd(s1.currentBill);
	};
	render() {
		let { products, sortCol, selectedFilter } = this.state;
		let arrStock = ["Yes", "No"];
		let arrCategory = ["Beverages", "Chocolates", "Biscuits"];
		let arrPrice = ["<10", "10-20", ">20"];
		if (this.props.close === 1) {
			this.handleClose();
			this.props.onClose();
		}
		let arr = products;
		if (selectedFilter.category != "") {
			if (selectedFilter.category === "Select Category") {
				arr = arr;
			} else {
				arr = arr.filter((ele) => {
					return ele.category === selectedFilter.category;
				});
			}
		}
		if (selectedFilter.instock != "") {
			if (selectedFilter.instock === "Select In Stock") {
				arr = arr;
			} else {
				arr = arr.filter((ele) => {
					return ele.instock === selectedFilter.instock;
				});
			}
		}
		if (selectedFilter.price != "") {
			if (selectedFilter.price === "<10") {
				arr = arr.filter((ele) => {
					return ele.price < 10;
				});
			} else if (selectedFilter.price === "10-20") {
				arr = arr.filter((ele) => {
					return ele.price >= 10 && ele.price <= 20;
				});
			} else if (selectedFilter.price === ">20") {
				arr = arr.filter((ele) => {
					return ele.price > 20;
				});
			} else {
				arr = arr;
			}
		}
		return (
			<React.Fragment>
				<h3 className='text-center'>Product List</h3>
				<div className='row mb-3'>
					<div className='col-3'>
						<h5>Filter Product by:</h5>
					</div>
					<div className='col-3'>
						<select
							className='form-control'
							name='category'
							value={selectedFilter["category"]}
							onChange={this.handleChange}
						>
							<option>Select Category</option>
							{arrCategory.map((ele) => {
								return <option>{ele}</option>;
							})}
						</select>
					</div>
					<div className='col-3'>
						<select
							className='form-control'
							name='instock'
							value={selectedFilter["instock"]}
							onChange={this.handleChange}
						>
							<option>Select In Stock</option>
							{arrStock.map((ele) => {
								return <option>{ele}</option>;
							})}
						</select>
					</div>
					<div className='col-3'>
						<select
							className='form-control'
							name='price'
							value={selectedFilter["price"]}
							onChange={this.handleChange}
						>
							<option>Select Price Range</option>
							{arrPrice.map((ele) => {
								return <option>{ele}</option>;
							})}
						</select>
					</div>
				</div>
				<div className='row border bg-dark text-white'>
					<div className='col-2' onClick={() => this.handleSort(1)}>
						Code{sortCol === 1 ? "(X)" : ""}
					</div>
					<div className='col-2' onClick={() => this.handleSort(2)}>
						Product{sortCol === 2 ? "(X)" : ""}
					</div>
					<div className='col-2' onClick={() => this.handleSort(3)}>
						Category{sortCol === 3 ? "(X)" : ""}
					</div>
					<div className='col-2' onClick={() => this.handleSort(4)}>
						Price{sortCol === 4 ? "(X)" : ""}
					</div>
					<div className='col-2' onClick={() => this.handleSort(5)}>
						In Stock{sortCol === 5 ? "(X)" : ""}
					</div>
					<div className='col-2'></div>
				</div>
				{arr.map((ele, index) => {
					return (
						<div className='row border'>
							<div className='col-2'>{ele.code}</div>
							<div className='col-2'>{ele.prod}</div>
							<div className='col-2'>{ele.category}</div>
							<div className='col-2'>{ele.price}</div>
							<div className='col-2'>{ele.instock}</div>
							<div className='col-2'>
								<button
									className='btn btn-secondary'
									onClick={() => this.handleAddToBill(ele.code)}
								>
									Add to bill
								</button>
							</div>
						</div>
					);
				})}
			</React.Fragment>
		);
	}
}
export default A8SectionAProducts;
