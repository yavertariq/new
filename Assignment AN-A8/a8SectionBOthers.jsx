import React, { Component } from "react";

class A8SectionBOthers extends Component {
	state = {
		others: this.props.others,
		cart: this.props.cart,
	};
	handleAdd = (index) => {
		let s1 = { ...this.state };
		let ent = s1.others[index];
		ent.quantity = 1;
		s1.cart.push(ent);
		this.setState(s1);
	};
	handleChangeQuantity = (index, val) => {
		let s1 = { ...this.state };
		s1.cart[index].quantity += val;
		if (s1.cart[index].quantity === 0) s1.cart.splice(index, 1);
		this.setState(s1);
		this.props.onAddToCart(s1.cart);
	};
	render() {
		let { others, cart } = this.state;
		if (others.length != this.props.others.length) {
			let s1 = { ...this.state };
			s1.others = this.props.others;
			this.setState(s1);
		}
		return (
			<div className='row border bg-light'>
				<div className='col-8'>
					<div className='row'>
						{others.map((ele, index) => {
							let ind = cart.findIndex((elem) => {
								return elem.id === ele.id;
							});
							return (
								<div className='col-6 border text-center'>
									<img src={ele.image} className='img-fluid m-1'></img>
									<h5 className='font-weight-bold'>{ele.name}</h5>
									<br></br>
									{ele.desc}
									<br></br>
									{ind >= 0 ? (
										<React.Fragment>
											<button
												className='btn btn-danger'
												onClick={() => this.handleChangeQuantity(ind, -1)}
											>
												-
											</button>
											<button className='btn btn-secondary' disabled>
												{ele.quantity}
											</button>
											<button
												className='btn btn-success'
												onClick={() => this.handleChangeQuantity(ind, 1)}
											>
												+
											</button>
										</React.Fragment>
									) : (
										<button
											className='btn btn-primary'
											onClick={() => this.handleAdd(index)}
										>
											Add To Cart
										</button>
									)}
								</div>
							);
						})}
					</div>
				</div>

				<div className='col-4 text-center'>
					{cart.length === 0 ? (
						<h3>Cart is empty</h3>
					) : (
						<React.Fragment>
							<h3>Cart</h3>
							{cart.map((ele, index) => {
								return (
									<div className='row border'>
										<div className='col-4 text-center my-auto'>
											<img src={ele.image} className='img-fluid'></img>
										</div>
										<div className='col-8 text-left'>
											<h5 className='font-weight-bold'>{ele.name}</h5>
											{ele.desc}
											<br></br>

											<button
												className='btn btn-danger'
												onClick={() => this.handleChangeQuantity(index, -1)}
											>
												-
											</button>
											<button className='btn btn-secondary' disabled>
												{ele.quantity}
											</button>
											<button
												className='btn btn-success'
												onClick={() => this.handleChangeQuantity(index, 1)}
											>
												+
											</button>
										</div>
									</div>
								);
							})}
						</React.Fragment>
					)}
				</div>
			</div>
		);
	}
}
export default A8SectionBOthers;
