import React, { Component } from "react";
import http from "../../services/httpService";

class ViewFacultyCourses extends Component {
	state = { courses: [] };
	async componentDidMount() {
		let response = await http.get("/login");
		let user = response;
		response = await http.get(`/getCourses?q=${user.id}`);
		console.log(response);
		this.setState({ courses: response.data });
	}
	handleViewLectures = (id) => {
		this.props.history.push(`/course/${id}`);
	};
	render() {
		let { courses } = this.state;

		return (
			<div className='container mt-5'>
				<h3>Your Courses</h3>
				<div className='row p-2 border bg-dark text-white'>
					<div className='col-2'>Id</div>
					<div className='col-3'>Title</div>
					<div className='col-3'>Student Id's</div>
				</div>
				{courses.length > 0 &&
					courses.map((ele) => {
						let arr = ele.Students.split("$");
						let str = arr.join(",");
						return (
							<div className='row p-2 border bg-light'>
								<div className='col-2'>{ele.id}</div>
								<div className='col-3'>{ele.Title}</div>
								<div className='col-3'>{str}</div>
								<div className='col-3'>
									<button
										className='btn btn-primary btn-sm'
										onClick={() => this.handleViewLectures(ele.id)}
									>
										View Lectures
									</button>
								</div>
							</div>
						);
					})}
			</div>
		);
	}
}
export default ViewFacultyCourses;
