var mysql = require("mysql");

var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "",
	database: "AssignmentDb",
});

con.connect(function (err) {
	if (err) throw err;
	console.log("Connected");
});

/*con.query(
	"CREATE TABLE Persons (id Integer,Name VARCHAR(15), Email VARCHAR(20), Password VARCHAR(15), Role VARCHAR(15))",
	function (err, result) {
		if (err) throw err;
		console.log("Table Created Persons");
	}
);
con.query(
	"CREATE TABLE Courses (id Integer,Title VARCHAR(15), FacultyId Integer,Students Integer)",
	function (err, result) {
		if (err) throw err;
		console.log("Table Created Courses");
	}
);*/
con.query(
	"CREATE TABLE Lectures (id Integer,CourseId Integer, Topic VARCHAR(15), Date VARCHAR(15), Attendance VARCHAR(30))",
	function (err, result) {
		if (err) throw err;
		console.log("Table Created Lectures");
	}
);
