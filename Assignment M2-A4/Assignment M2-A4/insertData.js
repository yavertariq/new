var mysql = require("mysql");
var csvtojson = require("csvtojson");

var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "",
	database: "AssignmentDb",
});

con.connect(function (err) {
	if (err) throw err;
	console.log("Connected");
});
/*
csvtojson()
	.fromFile("persons.csv")
	.then(function (json) {
		console.log(json);
		let keys = Object.keys(json[0]);
		let data = json.map((ele) => {
			let arr = keys.map((key) => {
				return ele[key];
			});
			return arr;
		});
		con.query(
			"INSERT INTO Persons (id,Name,Email,Password,Role) VALUES ?",
			[data],
			function (err, result) {
				if (err) throw err;
				console.log("Rows Inserted: ", result.affectedRows);
			}
		);
	});

csvtojson()
	.fromFile("courses.csv")
	.then(function (json) {
		console.log(json);
		let keys = Object.keys(json[0]);
		let data = json.map((ele) => {
			let arr = keys.map((key) => {
				return ele[key];
			});
			return arr;
		});
		console.log(data);
		con.query(
			"INSERT INTO Courses (id,Title,FacultyId,Students) VALUES ?",
			[data],
			function (err, result) {
				if (err) throw err;
				console.log("Rows Inserted: ", result.affectedRows);
			}
		);
	});
	*/
csvtojson()
	.fromFile("lectures.csv")
	.then(function (json) {
		console.log(json);
		let keys = Object.keys(json[0]);
		let data = json.map((ele) => {
			let arr = keys.map((key) => {
				return ele[key];
			});
			return arr;
		});
		console.log(data);
		con.query(
			"INSERT INTO Lectures (id,CourseId , Topic , Date , Attendance) VALUES ?",
			[data],
			function (err, result) {
				if (err) throw err;
				console.log("Rows Inserted: ", result.affectedRows);
			}
		);
	});
