import userEvent from "@testing-library/user-event";
import React, { Component } from "react";
import http from "../../services/httpService";

class Welcome extends Component {
	state = { user: {} };
	async componentDidMount() {
		let response = await http.get("/login");
		this.setState({ user: response });
	}
	render() {
		let { user } = this.state;
		return (
			<div className='container text-center mt-4'>
				<h3>Welcome to {user.Role} Portal</h3>
			</div>
		);
	}
}
export default Welcome;
