import React, { Component } from "react";
import http from "../../services/httpService";

class LeftPanel extends Component {
	state = { courses: [], options: [] };
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (input.checked) {
			s1.options.push(input.value);
		} else {
			let index = s1.options.findIndex((ele) => {
				console.log(ele, input.value);
				return ele === +input.value;
			});
			console.log(index);
			s1.options.splice(index, 1);
		}
		this.props.onOptionChange(s1.options);
	};
	async componentDidMount() {
		let id = this.props.id;
		let response = await http.get(`/getStudentCourses?id=${id}`);
		let s1 = { ...this.state };
		console.log(response);
		s1.courses = response.data;
		s1.options = this.props.selectedCourses;
		console.log(this.props.selectedCourses);
		this.setState(s1);
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.componentDidMount();
	}
	render() {
		let { courses } = this.state;
		let checked = this.props.selectedCourses;
		console.log(checked);
		return (
			<div className='container mt-5'>
				<div className='row'>
					<div className='col-12 bg-light border p-3'>
						<h4>Courses</h4>
					</div>
					{courses.length > 0 &&
						courses.map((ele) => {
							return (
								<div className='form-check col-12 p-2 border'>
									<input
										className='form-check-input ml-2'
										type='checkbox'
										value={ele.id}
										checked={checked.indexOf(+ele.id) >= 0 ? true : false}
										onChange={this.handleChange}
									></input>
									<label className='form-check-label ml-4'>{ele.Title}</label>
								</div>
							);
						})}
				</div>
			</div>
		);
	}
}
export default LeftPanel;
