import React, { Component } from "react";
import http from "../../services/httpService";

class ViewLectures extends Component {
	state = { lectures: [] };
	async componentDidMount() {
		let { id } = this.props.match.params;
		let response = await http.get(`/getLectures?f=${id}`);
		this.setState({ lectures: response.data });
	}
	handleAddNewLecture = () => {
		let { id } = this.props.match.params;
		this.props.history.push(`/courses/addLecture/${id}`);
	};
	handleAttendance = (id) => {
		this.props.history.push(`/courses/attendance?q=${id}`);
	};
	render() {
		let { lectures } = this.state;
		return (
			<div className='container mt-5'>
				<h3>Lectures</h3>
				<div className='row p-2 bg-dark text-white border'>
					<div className='col-1'>Id</div>
					<div className='col-2'>Course Id</div>
					<div className='col-3'>Topic</div>
					<div className='col-2'>Date</div>
					<div className='col-2'>Attendance</div>
				</div>
				{lectures.length > 0 &&
					lectures.map((ele) => {
						let arr = ele.Attendance.split("$");
						let str = arr.join(",");
						return (
							<div className='row p-2 bg-light border'>
								<div className='col-1'>{ele.id}</div>
								<div className='col-2'>{ele.CourseId}</div>
								<div className='col-3'>{ele.Topic}</div>
								<div className='col-2'>{ele.Date}</div>
								<div className='col-2'>{str}</div>
								<div className='col-2'>
									<button
										className='btn btn-warning btn-sm ml-2'
										onClick={() => this.handleAttendance(ele.id)}
									>
										Mark Attendance
									</button>
								</div>
							</div>
						);
					})}
				<button
					className='btn btn-primary mt-4'
					onClick={() => this.handleAddNewLecture()}
				>
					Add New Lecture
				</button>
			</div>
		);
	}
}
export default ViewLectures;
