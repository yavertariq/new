import React, { Component } from "react";
import { Link, Route, Switch, Redirect } from "react-router-dom";
import CreatePerson from "./createPerson";
import Login from "./login";
import http from "../../services/httpService";
import Welcome from "./welcome";
import Logout from "./logout";
import ViewAllPerson from "./viewAllPersons";
import CreateCourse from "./createCourse";
import ViewAllCourses from "./viewAllCourses";
import ViewFacultyCourses from "./viewFacultyCourses";
import ViewLectures from "./viewFacultyLectures";
import AddNewLecture from "./addNewLecture";
import Attendance from "./attendance";
import ViewAllCoursesStudents from "./viewAllCoursesStudents";
import AllLecturesStudent from "./viewAllLecturesStudent";
import StudentAttendance from "./viewStudentAttendance";
import Error from "./error";

class MainComponent extends Component {
	state = { user: {} };
	async componentDidMount() {
		let response = await http.get("/login");
		console.log(response);
		this.setState({ user: response });
	}

	render() {
		let { user } = this.state;
		console.log(user);
		console.log(user !== "Error");
		let keys = Object.keys(user);
		return (
			<React.Fragment>
				<nav className='navbar navbar-expand-sm navbar-dark bg-dark'>
					<Link className='navbar-brand' to='/'>
						Portal
					</Link>
					{user !== "Error" && user.Role === "Admin" && (
						<ul className='navbar-nav'>
							<li className='nav-item'>
								<Link className='nav-link' to='/newPerson'>
									Create Person
								</Link>
							</li>
							<li className='nav-item'>
								<Link className='nav-link' to='/viewAllPerson'>
									View All Persons
								</Link>
							</li>
							<li className='nav-item'>
								<Link className='nav-link' to='/createCourse'>
									Create Course
								</Link>
							</li>
							<li className='nav-item'>
								<Link className='nav-link' to='/viewAllCourses'>
									View All Courses
								</Link>
							</li>
						</ul>
					)}
					{user !== "Error" && user.Role === "Faculty" && (
						<ul className='navbar-nav'>
							<li className='nav-item'>
								<Link className='nav-link' to='/viewFacultyCourses'>
									View Courses
								</Link>
							</li>
						</ul>
					)}
					{user !== "Error" && user.Role === "Student" && (
						<ul className='navbar-nav'>
							<li className='nav-item'>
								<Link className='nav-link' to='/student/viewAllCourses'>
									View All Courses
								</Link>
							</li>
							<li className='nav-item'>
								<Link className='nav-link' to={`/allLectures/${user.id}`}>
									View All Lectures
								</Link>
							</li>
						</ul>
					)}
					{user !== "Error" && (
						<ul className='navbar-nav ml-auto'>
							<li className='nav-item'>
								<Link className='nav-link' to='/logout'>
									Logout
								</Link>
							</li>
						</ul>
					)}
				</nav>
				<Switch>
					<Route
						path='/studentAttendance'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Student" ? (
									<StudentAttendance {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/allLectures/:id'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Student" ? (
									<AllLecturesStudent {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/student/viewAllCourses'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Student" ? (
									<ViewAllCoursesStudents {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/courses/attendance'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Faculty" ? (
									<Attendance {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/courses/addLecture/:id'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Faculty" ? (
									<AddNewLecture {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/course/:id'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Faculty" ? (
									<ViewLectures {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/viewFacultyCourses'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Faculty" ? (
									<ViewFacultyCourses {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/viewAllCourses'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Admin" ? (
									<ViewAllCourses {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/createCourse'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Admin" ? (
									<CreateCourse {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/viewAllPerson'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Admin" ? (
									<ViewAllPerson {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/newPerson'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								user.Role === "Admin" ? (
									<CreatePerson {...props} />
								) : (
									<Redirect to='/error' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/welcome'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								<Welcome {...props} />
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/logout'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								<Logout {...props} />
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/login'
						render={(props) =>
							user !== "Error" && keys.length > 0 ? (
								<Welcome {...props} />
							) : (
								<Login {...props} />
							)
						}
					/>
					<Route path='/error' component={Error} />
					<Redirect from='/' to={user !== "Error" ? "/welcome" : "/login"} />
				</Switch>
			</React.Fragment>
		);
	}
}
export default MainComponent;
