import React, { Component } from "react";
import http from "../../services/httpService";

class ViewAllCoursesStudents extends Component {
	state = { courses: [], student: {} };
	async componentDidMount() {
		let response = await http.get("/getCourses");
		let s1 = { ...this.state };
		s1.courses = response.data;
		response = await http.get("/login");
		s1.student = response;
		this.setState(s1);
	}
	async putData(url, obj) {
		let response = await http.put(url, obj);
		this.componentDidMount();
	}
	handleEnroll = (id) => {
		this.putData(`/updateEnrollment/${id}`, this.state.student);
	};
	render() {
		let { courses, student } = this.state;
		let id = `${student.id}`;
		return (
			<div className='container mt-5'>
				<h3>All Courses</h3>
				<div className='row bg-dark p-2 text-white'>
					<div className='col-1'>Id</div>
					<div className='col-2'>Title</div>
					<div className='col-2'>Faculty Id</div>
					<div className='col-4'>Student Id's</div>
				</div>
				{courses.length > 0 &&
					courses.map((ele) => {
						let arr = ele.Students.split("$");
						let str = arr.join(",");
						console.log(arr);
						return (
							<div className='row p-2 bg-light border'>
								<div className='col-1'>{ele.id}</div>
								<div className='col-2'>{ele.Title}</div>
								<div className='col-2'>{ele.FacultyId}</div>
								<div className='col-4'>{str}</div>
								<div className='col-2'>
									{arr.indexOf(id) >= 0 ? (
										"Enrolled"
									) : (
										<button
											className='btn btn-primary btn-sm'
											onClick={() => this.handleEnroll(ele.id)}
										>
											Enroll
										</button>
									)}
								</div>
							</div>
						);
					})}
			</div>
		);
	}
}
export default ViewAllCoursesStudents;
