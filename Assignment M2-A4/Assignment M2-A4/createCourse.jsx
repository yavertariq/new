import React, { Component } from "react";
import http from "../../services/httpService";

class CreateCourse extends Component {
	state = { persons: [], course: { Students: "" } };

	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (input.name === "Faculty") {
			let person = s1.persons.find((ele) => {
				return ele.Name === input.value;
			});
			s1.course[input.name] = person.id;
		} else s1.course[input.name] = input.value;
		this.setState(s1);
	};

	async postData(url, obj) {
		let status = 200;
		try {
			let response = await http.post(url, obj);
		} catch (ex) {
			if (ex && ex.response.data === 400) {
				status = 400;
				alert("Check Details");
			}
		}
		if (status === 200) {
			alert("Course Added to database");
			this.props.history.push("/welcome");
		}
	}

	async componentDidMount() {
		let response = await http.get("/viewPersons?q=faculty");
		console.log(response);
		this.setState({ persons: response.data });
	}
	handleAdd = (e) => {
		e.preventDefault();
		this.postData(`/addCourse`, this.state.course);
	};
	render() {
		let { persons, course } = this.state;
		let { Title = "", Faculty = "" } = course;
		return (
			<div className='container mt-5'>
				<h3>New Course Details</h3>
				<div className='form-group mt-4'>
					<label>Title:</label>
					<input
						className='form-control'
						name='Title'
						value={Title}
						onChange={this.handleChange}
					></input>
				</div>
				<div className='form-group'>
					<label>Faculty:</label>
					<select
						className='form-control'
						value={Faculty}
						name='Faculty'
						onChange={this.handleChange}
					>
						<option selected disabled value=''>
							Select Faculty
						</option>
						{persons.map((ele) => {
							return <option>{ele.Name}</option>;
						})}
					</select>
				</div>

				<button className='btn btn-primary mt-3' onClick={this.handleAdd}>
					Add
				</button>
			</div>
		);
	}
}
export default CreateCourse;
