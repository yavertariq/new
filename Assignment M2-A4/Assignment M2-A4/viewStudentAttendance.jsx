import React, { Component } from "react";
import http from "../../services/httpService";
import queryString from "query-string";

class StudentAttendance extends Component {
	state = { lecture: {}, student: {} };
	async componentDidMount() {
		let queryParams = queryString.parse(this.props.location.search);
		let response = await http.get(`/getLectures?q=${queryParams.id}`);
		console.log(response);
		let s1 = { ...this.state };
		s1.lecture = response.data[0];
		response = await http.get("/login");
		s1.student = response;
		this.setState(s1);
	}
	render() {
		let { lecture, student } = this.state;
		let atd = "Absent";
		let keys = Object.keys(lecture);
		if (keys.length > 0) {
			let arr = lecture.Attendance.split("$");
			let id = `${student.id}`;
			if (arr.indexOf(id) >= 0) {
				atd = "Present";
			}
		}
		return (
			<div className='container mt-5'>
				<h3>Lecture: {lecture.Topic}</h3>
				<h4>Date: {lecture.Date}</h4>
				<h4>Attendance: {atd}</h4>
			</div>
		);
	}
}
export default StudentAttendance;
