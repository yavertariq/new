import React, { Component } from "react";
import http from "../../services/httpService";

class AddNewLecture extends Component {
	state = { lecture: {} };
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (input.name === "Date") {
			if (input.value.length === 2 || input.value.length === 5) {
				s1.lecture.Date = input.value + "/";
			} else {
				s1.lecture.Date = input.value;
			}
		} else s1.lecture[input.name] = input.value;
		this.setState(s1);
	};
	checkInputs = (lecture) => {
		let { Topic = "", Date = "" } = lecture;

		if (Topic) {
			if (Date) {
				let arr = Date.split("/");
				if (+arr[1] <= 12 && +arr[0] <= 31) {
					return true;
				} else return false;
			} else return false;
		} else {
			return false;
		}
	};
	async postData(url, obj) {
		let response = await http.post(url, obj);
	}
	handleAdd = (e) => {
		e.preventDefault();
		let { id } = this.props.match.params;
		let s1 = { ...this.state };
		if (this.checkInputs(s1.lecture)) {
			this.postData(`/addLecture/${id}`, s1.lecture);
			alert("Lecture Added");
		} else {
			alert("Invalid Input");
		}
	};
	render() {
		let { Topic = "", Date = "" } = this.state.lecture;
		return (
			<div className='container mt-5'>
				<h3>Add New Lecture</h3>
				<div className='form-group mt-4'>
					<label>Topic:</label>
					<span className='text-danger'>*</span>
					<input
						className='form-control'
						name='Topic'
						value={Topic}
						onChange={this.handleChange}
					></input>
				</div>
				<div className='form-group mt-4'>
					<label>Date:</label>
					<span className='text-danger'>*</span>
					<input
						className='form-control'
						placeholder='DD/MM/YYYY'
						name='Date'
						value={Date}
						onChange={this.handleChange}
					></input>
				</div>
				<button className='btn btn-primary mt-4' onClick={this.handleAdd}>
					Add
				</button>
			</div>
		);
	}
}
export default AddNewLecture;
