import React, { Component } from "react";
import http from "../../services/httpService";

class Login extends Component {
	state = { user: {} };
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.user[input.name] = input.value;
		this.setState(s1);
	};
	async postData(url, obj) {
		let response;
		let check = true;
		try {
			response = await http.post(url, obj);
			console.log(response);
		} catch (ex) {
			if (ex && ex.response.status === 401) {
				alert("Check Login Credentials");
				check = false;
			}
		}
		if (check) {
			localStorage.setItem("user", response.data.token);
			window.location = "/";
		}
	}
	handleEnter = (e) => {
		e.preventDefault();
		this.postData(`/login`, this.state.user);
	};
	render() {
		let { email = "", password = "" } = this.state.user;
		return (
			<div className='container mt-5 text-center'>
				<h3>Login</h3>
				<div className='row mt-5'>
					<div className='col-4 text-right'>
						<h4>Email:</h4>
					</div>
					<input
						className='form-control col-4'
						name='email'
						value={email}
						onChange={this.handleChange}
					></input>
				</div>
				<div className='row mt-4'>
					<div className='col-4 text-right'>
						<h4>Password:</h4>
					</div>
					<input
						className='form-control col-4'
						type='password'
						name='password'
						value={password}
						onChange={this.handleChange}
					></input>
				</div>
				<button className='btn btn-primary mt-4' onClick={this.handleEnter}>
					Enter
				</button>
			</div>
		);
	}
}
export default Login;
