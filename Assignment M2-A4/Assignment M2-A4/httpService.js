import axios from "axios";

const baseURL = "http://localhost:2410";

async function get(url) {
	const user = localStorage.getItem("user");
	if (url === "/login") {
		try {
			var { data: resp } = await axios.get(baseURL + url, {
				headers: { Authorization: user },
			});
			return resp;
		} catch (ex) {
			return "Error";
		}
	} else {
		console.log(baseURL + url);
		return axios.get(baseURL + url);
	}
}

function post(url, obj) {
	console.log(baseURL + url, obj);
	return axios.post(baseURL + url, obj);
}
function put(url, obj) {
	return axios.put(baseURL + url, obj);
}
async function deleteReq() {
	const user = localStorage.getItem("user");
	try {
		var { data: resp } = await axios.delete(`${baseURL}/logout`, {
			headers: { Authorization: user },
		});
		localStorage.removeItem("user");
		return resp;
	} catch (ex) {
		return "Error";
	}
}
export default {
	get,
	post,
	put,
	deleteReq,
};
