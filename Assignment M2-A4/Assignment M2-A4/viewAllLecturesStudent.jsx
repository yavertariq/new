import React, { Component } from "react";
import http from "../../services/httpService";
import LeftPanel from "./studentLecturesLeftPanel";
import queryString from "query-string";

class AllLecturesStudent extends Component {
	state = { lectures: [], options: [] };
	async componentDidMount() {
		let { id } = this.props.match.params;
		let queryParams = queryString.parse(this.props.location.search);
		let response;
		let s1 = { ...this.state };
		if (queryParams.courseIds) {
			response = await http.get(
				`/getStudentLectures?id=${id}&courseIds=${queryParams.courseIds}`
			);
			let arr = queryParams.courseIds.split(",");
			s1.options = arr.map((ele) => {
				return +ele;
			});
		} else {
			response = await http.get(`/getStudentLectures?id=${id}`);
		}
		console.log(response);
		s1.lectures = response.data;
		this.setState(s1);
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.componentDidMount();
	}
	handleOptionChange = (options) => {
		console.log(options);
		let str = options.join(",");
		let { id } = this.props.match.params;
		this.props.history.push(`/allLectures/${id}?courseIds=${str}`);
	};
	handleViewAttendance = (id) => {
		this.props.history.push(`/studentAttendance?id=${id}`);
	};
	render() {
		let { lectures, options } = this.state;
		let courses = lectures.reduce((acc, curr) => {
			if (acc.indexOf(curr.CourseId) < 0) {
				acc.push(curr.CourseId);
			}
			return acc;
		}, []);
		let { id } = this.props.match.params;
		return (
			<div className='container mt-5'>
				<div className='row'>
					<div className='col-3'>
						<LeftPanel
							id={id}
							selectedCourses={options.length > 0 ? options : courses}
							onOptionChange={this.handleOptionChange}
						/>
					</div>
					<div className='col-9'>
						<h3>Your Lectures</h3>
						<div className='row p-2 bg-dark text-white border'>
							<div className='col-1'>Id</div>
							<div className='col-2'>Course Id</div>
							<div className='col-3'>Topic</div>
							<div className='col-2'>Date</div>
						</div>
						{lectures.length > 0 &&
							lectures.map((ele) => {
								return (
									<div
										className='row p-2 bg-light border'
										style={{ cursor: "pointer" }}
										onClick={() => this.handleViewAttendance(ele.id)}
									>
										<div className='col-1'>{ele.id}</div>
										<div className='col-2'>{ele.CourseId}</div>
										<div className='col-3'>{ele.Topic}</div>
										<div className='col-2'>{ele.Date}</div>
									</div>
								);
							})}
					</div>
				</div>
			</div>
		);
	}
}
export default AllLecturesStudent;
