import React, { Component } from "react";
import http from "../../services/httpService";

class ViewAllCourses extends Component {
	state = { courses: [] };
	async componentDidMount() {
		let response = await http.get("/getCourses");
		this.setState({ courses: response.data });
	}
	render() {
		let { courses } = this.state;
		return (
			<div className='container mt-5'>
				<h3>All Courses</h3>
				<div className='row bg-dark p-2 text-white'>
					<div className='col-2'>Id</div>
					<div className='col-4'>Title</div>
					<div className='col-2'>Faculty Id</div>
					<div className='col-4'>Student Id's</div>
				</div>
				{courses.length > 0 &&
					courses.map((ele) => {
						let arr = ele.Students.split("$");
						let str = arr.join(",");
						return (
							<div className='row p-2 bg-light border'>
								<div className='col-2'>{ele.id}</div>
								<div className='col-4'>{ele.Title}</div>
								<div className='col-2'>{ele.FacultyId}</div>
								<div className='col-4'>{str}</div>
							</div>
						);
					})}
			</div>
		);
	}
}
export default ViewAllCourses;
