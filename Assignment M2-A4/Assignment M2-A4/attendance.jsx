import React, { Component } from "react";
import http from "../../services/httpService";
import queryString from "query-string";

class Attendance extends Component {
	state = { persons: [], lecture: {} };
	async componentDidMount() {
		let queryParams = queryString.parse(this.props.location.search);
		console.log(queryParams);
		let response = await http.get("/viewPersons?q=Student");
		let s1 = { ...this.state };
		s1.persons = response.data;
		response = await http.get(`/getLectures?q=${queryParams.q}`);
		console.log(response);
		s1.lecture = response.data[0];
		this.setState(s1);
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (input.checked) {
			let arr = s1.lecture.Attendance.split("$");
			if (arr[0] === "") {
				arr[0] = input.id;
			} else arr.push(input.id);
			s1.lecture.Attendance = arr.join("$");
		} else {
			let arr = s1.lecture.Attendance.split("$");
			let index = arr.findIndex((ele) => {
				return ele === input.id;
			});
			arr.splice(index, 1);
			s1.lecture.Attendance = arr.join("$");
		}
		this.setState(s1);
	};
	async putData(url, obj) {
		let response = await http.put(url, obj);
		this.props.history.push("/welcome");
	}
	handleUpdate = (e) => {
		e.preventDefault();
		this.putData("/updateAttendance", this.state.lecture);
	};
	render() {
		let { persons, lecture } = this.state;
		let arr = persons.length > 0 ? lecture.Attendance.split("$") : [];
		console.log(arr);
		return (
			<div className='container mt-5'>
				<h3>Lecture: {lecture.Topic}</h3>
				<h4 className='mt-4'>Attendance:</h4>
				{persons.map((ele) => {
					return (
						<div className='form-check'>
							<input
								id={ele.id}
								className='form-check-input'
								type='checkbox'
								checked={arr.indexOf(`${ele.id}`) >= 0 ? true : false}
								onChange={this.handleChange}
							></input>
							<label className='form-check-label'>{ele.Name}</label>
						</div>
					);
				})}
				<button className='btn btn-primary mt-4' onClick={this.handleUpdate}>
					Update
				</button>
			</div>
		);
	}
}
export default Attendance;
