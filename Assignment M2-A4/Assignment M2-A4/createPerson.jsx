import React, { Component } from "react";
import http from "../../services/httpService";

class CreatePerson extends Component {
	state = { person: {} };
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.person[input.name] = input.value;
		this.setState(s1);
	};
	async postData(url, obj) {
		let status = 200;
		try {
			let response = await http.post(url, obj);
			console.log(response);
		} catch (ex) {
			if (ex && ex.response.status === 400) {
				alert("Email already exists");
				status = 400;
			}
		}
		if (status === 200) {
			alert("Person added to Database");
			window.location = "/welcome";
		}
	}
	checkInput = (person) => {
		let { Name = "", Password = "", Email = "", Role = "" } = person;
		if (Name === "" || Password === "" || Email === "" || Role === "") {
			return false;
		} else return true;
	};
	handleAdd = (e) => {
		e.preventDefault();
		if (this.checkInput(this.state.person)) {
			this.postData("/newPerson", this.state.person);
		} else {
			alert("Enter all the details");
		}
	};
	render() {
		let { Name = "", Email = "", Password = "", Role = "" } = this.state.person;
		let roles = ["Admin", "Faculty", "Student"];
		return (
			<div className='container mt-5'>
				<h3>Create Person</h3>
				<div className='form-group mt-4'>
					<label>Name:</label>
					<input
						className='form-control'
						name='Name'
						value={Name}
						onChange={this.handleChange}
					></input>
				</div>
				<div className='form-group mt-4'>
					<label>Email:</label>
					<input
						className='form-control'
						name='Email'
						value={Email}
						onChange={this.handleChange}
					></input>
				</div>
				<div className='form-group mt-4'>
					<label>Password:</label>
					<input
						className='form-control'
						type='password'
						name='Password'
						value={Password}
						onChange={this.handleChange}
					></input>
				</div>
				<div className='form-group'>
					<label>Role:</label>
					<select
						className='form-control'
						name='Role'
						value={Role}
						onChange={this.handleChange}
					>
						<option selected disabled value=''>
							Select Role
						</option>
						{roles.map((ele) => {
							return <option>{ele}</option>;
						})}
					</select>
				</div>
				<div className='container text-center '>
					<button className='btn btn-primary' onClick={this.handleAdd}>
						Add
					</button>
				</div>
			</div>
		);
	}
}
export default CreatePerson;
