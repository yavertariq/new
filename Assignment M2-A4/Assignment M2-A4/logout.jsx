import React, { Component } from "react";
import http from "../../services/httpService";

class Logout extends Component {
	async componentDidMount() {
		let response = await http.deleteReq("/logout");
		window.location = "/login";
	}

	render() {
		return <div></div>;
	}
}
export default Logout;
