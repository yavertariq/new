import React, { Component } from "react";
import http from "../../services/httpService";

class ViewAllPerson extends Component {
	state = { persons: [] };

	async componentDidMount() {
		let response = await http.get(`/viewPersons`);
		console.log(response);
		this.setState({ persons: response.data });
	}

	render() {
		let { persons } = this.state;
		return (
			<div className='container mt-5'>
				<div className='row p-2 border bg-dark text-white'>
					<div className='col-2'>Id</div>
					<div className='col-3'>Name</div>
					<div className='col-4'>Email</div>
					<div className='col-2'>Role</div>
				</div>
				{persons.length > 0 &&
					persons.map((ele) => {
						return (
							<div className='row p-2 border bg-light'>
								<div className='col-2'>{ele.id}</div>
								<div className='col-3'>{ele.Name}</div>
								<div className='col-4'>{ele.Email}</div>
								<div className='col-2'>{ele.Role}</div>
							</div>
						);
					})}
			</div>
		);
	}
}
export default ViewAllPerson;
