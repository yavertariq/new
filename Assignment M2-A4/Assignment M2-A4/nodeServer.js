var express = require("express");
var mysql = require("mysql");
var jwt = require("jsonwebtoken");
var passport = require("passport");
var passportJWT = require("passport-jwt");
var cfg = require("./config");
const bodyParser = require("body-parser");
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var params = {
	secretOrKey: cfg.jwtSecret,
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin,X-Requested-With,Content-Type,Accept"
	);
	res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
	res.header("Access-Control-Allow-Credentials", "true");
	res.header(
		"Access-Control-Allow-Headers",
		"Access-Control-Allow-Headers,Origin,Accept,Authorization,X-Requested-With,Content-Type,Access-Control-Request-Headers"
	);
	res.header("Access-Control-Expose-Headers", "*");
	next();
});
app.use(bodyParser.json());
passport.initialize();
const port = 2410;
app.listen(port, () => console.log("App listening at port: ", port));

var con = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "",
	database: "AssignmentDb",
});

con.connect(function (err) {
	if (err) throw err;
	console.log("Connected");
});

let persons = [];

var strategy1 = new Strategy(params, function (payload, done) {
	con.query("SELECT * FROM Persons", function (err, result) {
		if (err) throw err;
		persons = result;

		let id = persons.findIndex((obj) => payload.id === obj.id);
		var user = persons[+id] || null;
		if (user) {
			return done(null, {
				id: user.id,
				role: user.Role,
			});
		} else {
			return done(new Error("User not found"), null);
		}
	});
});

var strategy2 = new Strategy(params, function (payload, done) {
	con.query("SELECT * FROM Persons", function (err, result) {
		if (err) throw err;
		persons = result;

		let id = persons.findIndex((obj) => payload.id === obj.id);
		var user = persons[+id] || null;
		if (user) {
			return done(null, {
				role: user.Role,
				id: user.id,
			});
		} else {
			return done(new Error("User not found"), null);
		}
	});
});
passport.use("local.one", strategy1);
passport.use("local.two", strategy2);
const jwtExpirySeconds = 300;

app.post("/login", function (req, res, payload) {
	con.query("SELECT * FROM Persons", function (err, result) {
		if (err) throw err;
		let persons = result;
		if (req.body.email && req.body.password) {
			var email = req.body.email;
			var password = req.body.password;
			var user = persons.find(function (u) {
				return u.Email === email && u.Password === password;
			});

			if (user) {
				var payload = {
					id: user.id,
				};
				var token = jwt.sign(payload, cfg.jwtSecret, {
					algorithm: "HS256",
					expiresIn: jwtExpirySeconds,
				});
				res.setHeader("X-Auth-Token", token);
				res.json({ success: true, token: "bearer " + token, role: user.role });
			} else {
				res.sendStatus(401);
			}
		} else {
			res.sendStatus(401);
		}
	});
});

app.get(
	"/login",
	passport.authenticate("local.one", { session: false }),
	function (req, res) {
		con.query("SELECT * FROM Persons", function (err, result) {
			if (err) throw err;
			let persons = result;
			console.log(result);
			res.json(persons[persons.findIndex((obj) => obj.id === req.user.id)]);
		});
	}
);
app.post("/newPerson", function (req, res) {
	let user = req.body;

	con.query("SELECT * FROM Persons", function (err, result) {
		if (err) throw err;
		let persons = result;

		let index = persons.findIndex((ele) => {
			return ele.Email === user.Email;
		});
		if (index < 0) {
			con.query("SELECT MAX(id) AS id FROM Persons", function (err, result) {
				if (err) throw err;
				let id = result["0"].id;
				let data = [];
				id = +id + 1;
				console.log(id);

				let keys = Object.keys(user);
				data = keys.map((key) => {
					return user[key];
				});
				data.unshift(id);
				console.log(data);
				con.query(
					"INSERT INTO Persons (id,Name,Email,Password,Role) VALUES (?)",
					[data],
					function (err, result) {
						if (err) throw err;
						console.log(result);
						console.log("Person Added to Database");
						res.send("Person Added to Database");
					}
				);
			});
		} else {
			res.status(400);
			res.send("Error email already exists");
		}
	});
});

app.get("/viewPersons", function (req, res) {
	let role = req.query.q;
	if (role) {
		con.query(
			`SELECT * FROM Persons WHERE Role='${role}'`,
			function (err, result) {
				if (err) throw err;
				res.send(result);
			}
		);
	} else {
		con.query("SELECT * FROM Persons", function (err, result) {
			if (err) throw err;
			res.send(result);
		});
	}
});
app.post("/addCourse", function (req, res) {
	let course = req.body;

	con.query("SELECT MAX(id) AS id FROM Courses", function (err, result) {
		if (err) throw err;
		let id = result["0"].id;
		let data = [];
		id = +id + 1;
		console.log(id);
		data.push(course.Title);
		data.push(course.Faculty);
		data.push("");
		data.unshift(id);
		console.log(data);
		con.query(
			"INSERT INTO Courses (id,Title,FacultyId,Students) VALUES (?)",
			[data],
			function (err, result) {
				if (err) throw err;
				console.log("Course added to Database");
				res.send("Course added to Database");
			}
		);
	});
});

app.get("/getCourses", function (req, res) {
	let facultyId = req.query.q;
	if (facultyId) {
		con.query(
			`SELECT * FROM Courses WHERE FacultyId='${facultyId}'`,
			function (err, result) {
				if (err) throw err;
				console.log(result);
				res.send(result);
			}
		);
	} else {
		con.query("SELECT * FROM Courses", function (err, result) {
			if (err) throw err;
			res.send(result);
		});
	}
});

app.get("/getLectures", function (req, res) {
	let id = req.query.f;
	let lecId = req.query.q;
	if (lecId) {
		con.query(
			`SELECT * FROM Lectures WHERE id=${lecId}`,
			function (err, result) {
				if (err) throw err;
				console.log(result);
				res.send(result);
			}
		);
	}
	if (id) {
		con.query(
			`SELECT * FROM Lectures WHERE CourseId=${id}`,
			function (err, result) {
				if (err) throw err;
				console.log(result);
				res.send(result);
			}
		);
	}
});

app.post("/addLecture/:id", function (req, res) {
	let courseId = req.params.id;
	let lecture = req.body;
	con.query("SELECT MAX(id) AS id FROM Lectures", function (err, result) {
		if (err) throw err;
		let id = result["0"].id;
		let data = [];
		id = +id + 1;
		let keys = Object.keys(lecture);
		data = keys.map((key) => {
			return lecture[key];
		});
		data.unshift(+courseId);
		data.unshift(id);
		data.push("");
		console.log(data);
		con.query(
			"INSERT INTO Lectures (id,CourseId , Topic , Date , Attendance) VALUES (?)",
			[data],
			function (err, result) {
				if (err) throw err;
				console.log("Lecture Added");
				res.send("Lecture Added");
			}
		);
	});
});

app.put("/updateAttendance", function (req, res) {
	let lecture = req.body;
	con.query(
		`UPDATE Lectures SET Attendance="${lecture.Attendance}" WHERE Topic="${lecture.Topic}"`,
		function (err, result) {
			if (err) throw err;
			console.log("Table Updated");
		}
	);
	con.query(
		`SELECT * FROM Courses WHERE id=${lecture.CourseId}`,
		function (err, result) {
			if (err) throw err;
			console.log(result);
			res.send("Table updated");
		}
	);
});

app.put("/updateEnrollment/:id", function (req, res) {
	let id = req.params.id;
	let student = req.body;
	con.query(`SELECT * FROM Courses WHERE id=${id}`, function (err, result) {
		if (err) throw err;
		let arr = result[0].Students.split("$");
		let stdId = `${student.id}`;
		if (arr[0] === "") {
			arr[0] = stdId;
		} else arr.push(stdId);
		let str = arr.join("$");
		con.query(
			`UPDATE Courses SET Students='${str}' WHERE id=${id}`,
			function (err, result) {
				if (err) throw err;
				console.log("Table Updated");
				res.send("Table Updated");
			}
		);
	});
});

app.get("/getStudentLectures", function (req, res) {
	let id = `${req.query.id}`;
	let courseIds = req.query.courseIds;
	let arr = [];
	let arr2 = [];
	if (courseIds) {
		arr = courseIds.split(",");
		arr2 = arr.map((ele) => {
			return +ele;
		});
	}
	con.query("SELECT * FROM Courses", function (err, result) {
		if (err) throw err;
		if (courseIds) {
			console.log("called custome");
			con.query(
				`SELECT * FROM Lectures WHERE CourseId IN (${arr2})`,
				function (err, result) {
					if (err) throw err;
					console.log(result);
					res.send(result);
				}
			);
		} else {
			console.log("called normal");
			let ids = result.reduce((acc, curr) => {
				let arr = curr.Students.split("$");
				if (arr.indexOf(id) >= 0) {
					acc.push(+curr.id);
				}
				return acc;
			}, []);
			con.query(
				`SELECT * FROM Lectures WHERE CourseId IN (${ids})`,
				function (err, result) {
					if (err) throw err;
					console.log(result);
					res.send(result);
				}
			);
		}
	});
});

app.get("/getStudentCourses", function (req, res) {
	let id = req.query.id;
	id = `${id}`;
	con.query(`SELECT * FROM Courses`, function (err, result) {
		if (err) throw err;
		let data = result.reduce((acc, curr) => {
			let arr = curr.Students.split("$");
			if (arr.indexOf(id) >= 0) {
				acc.push(curr);
			}
			return acc;
		}, []);
		console.log(data);
		res.send(data);
	});
});

var logout = require("express-passport-logout");
const { default: axios } = require("axios");
app.delete(
	"/logout",
	passport.authenticate("local.one", { session: false }),
	logout()
);
