import React, { Component } from "react";
import http from "./httpService";

class Register extends Component {
	state = {
		check: true,
		passCheck: "",
		user: {},
		errors: {
			name: "pop",
			password: "pop",
			passCheck: "pop",
			email: "pop",
			role: "pop",
		},
	};
	async postData(url, obj) {
		let response = await http.post(url, obj);
		window.location = "/admin";
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (input.name === "passCheck") {
			s1.passCheck = input.value;
		} else s1.user[input.name] = input.value;
		let error = this.validateInput(s1, input.name);
		s1.errors[input.name] = error;
		if (this.validateErrors(s1.errors)) {
			s1.check = false;
		} else {
			s1.check = true;
		}
		this.setState(s1);
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);

		return count === 0;
	};
	validateInput = (s1, name) => {
		let { user, passCheck } = s1;
		let error = "";
		if (name === "name") {
			error = user.name ? "" : "Cannot be Empty";
		} else if (name === "password") {
			error = user.password
				? user.password.length > 6
					? ""
					: "Password must be atleast 7 characters"
				: "Password must be atleast 7 characters";
		} else if (name === "passCheck") {
			error =
				user.password === passCheck
					? passCheck.length > 6
						? ""
						: "Password must be atleast 7 characters"
					: "Password does not match";
		} else if (name === "email") {
			error = this.checkEmail(user.email);
		} else {
			error = user.role ? "" : "Select a role";
		}
		return error;
	};
	checkEmail = (str) =>
		str
			? (str[0] >= "a" && str[0] <= "z") || (str[0] >= "A" && str[0] <= "Z")
				? str.indexOf("@") > 0
					? str[str.indexOf("@") + 1] !== "."
						? str.indexOf(".", str.indexOf("@") + 2) < 0
							? "Enter a valid email"
							: str[str.length - 1] === "."
							? "Enter a valid email"
							: ""
						: "Enter a valid email"
					: "Enter a valid email"
				: "Enter a valid email"
			: "Enter a valid email";
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		alert("User created successfully!");
		this.postData("/register", s1.user);
	};
	render() {
		let { check, user, passCheck, errors } = this.state;
		let { name = "", password = "", role = "", email = "" } = user;
		return (
			<div className='container mt-4' style={{ marginLeft: "15%" }}>
				<h3>Register</h3>
				<div className='form-group'>
					<label className='font-weight-bold'>
						Name<span className='text-danger'>*</span>
					</label>
					<input
						className='form-control col-10'
						name='name'
						placeholder=' Enter your name'
						value={name}
						onChange={this.handleChange}
					></input>
				</div>
				<div className='form-group'>
					<label className='font-weight-bold'>
						Password<span className='text-danger'>*</span>
					</label>
					<input
						className='form-control col-10'
						type='password'
						name='password'
						placeholder=' Enter password'
						value={password}
						onChange={this.handleChange}
					></input>
					{errors.password && errors.password !== "pop" && (
						<span
							className='form-control col-10'
							style={{ background: "pink", color: "#a5002f" }}
						>
							{errors.password}
						</span>
					)}
				</div>
				<div className='form-group'>
					<label className='font-weight-bold'>
						Confirm Password<span className='text-danger'>*</span>
					</label>
					<input
						className='form-control col-10'
						type='password'
						name='passCheck'
						placeholder=' Re-Enter your password'
						value={passCheck}
						onChange={this.handleChange}
					></input>
					{errors.passCheck && errors.passCheck !== "pop" && (
						<span
							className='form-control col-10'
							style={{ background: "pink", color: "#a5002f" }}
						>
							{errors.passCheck}
						</span>
					)}
				</div>
				<div className='form-group'>
					<label className='font-weight-bold'>
						Email<span className='text-danger'>*</span>
					</label>
					<input
						className='form-control col-10'
						name='email'
						placeholder=' Enter your email'
						value={email}
						onChange={this.handleChange}
					></input>
					{errors.email && errors.email !== "pop" && (
						<span
							className='form-control col-10'
							style={{ background: "pink", color: "#a5002f" }}
						>
							{errors.email}
						</span>
					)}
				</div>
				<div className='form-group'>
					<label className='font-weight-bold'>
						Role<span className='text-danger'>*</span>
					</label>
					<div className='form-check'>
						<input
							className='form-check-input'
							type='radio'
							name='role'
							value='student'
							checked={role === "student"}
							onChange={this.handleChange}
						></input>
						<label className='form-check-label'>Student</label>
					</div>
					<div className='form-check'>
						<input
							className='form-check-input'
							type='radio'
							name='role'
							value='faculty'
							checked={role === "faculty"}
							onChange={this.handleChange}
						></input>
						<label className='form-check-label'>Faculty</label>
						<br></br>
						{errors.role && errors.role !== "pop" && (
							<span
								className='form-control col-10'
								style={{ background: "pink", color: "#a5002f" }}
							>
								{errors.role}
							</span>
						)}
					</div>
					<button
						className='btn btn-primary'
						onClick={this.handleSubmit}
						disabled={check}
					>
						Register
					</button>
				</div>
			</div>
		);
	}
}
export default Register;
