import React, { Component } from "react";
import http from "./httpService";
import queryString from "query-string";
import LeftPanel from "./leftPanel";

class AllFaculties extends Component {
	state = { data: {} };
	async fetch() {
		let queryParams = queryString.parse(this.props.location.search);
		let s1 = { ...this.state };
		let searchStr = this.makeSearchString(queryParams);
		console.log(searchStr);
		let response = await http.get(`/getFaculties?${searchStr}`);
		let { data } = response;
		s1.data = data;
		this.setState(s1);
	}
	componentDidMount() {
		this.fetch();
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.fetch();
	}
	handlePage = (val) => {
		let queryParams = queryString.parse(this.props.location.search);
		let newPage = +queryParams.page + val;
		queryParams.page = newPage;
		let searchStr = this.makeSearchString(queryParams);
		this.props.history.push(`/allFaculties?${searchStr}`);
	};
	handleOptionChange = (options) => {
		let searchStr = this.makeSearchString(options);
		this.props.history.push(`/allFaculties?${searchStr}`);
	};
	makeSearchString = (options) => {
		let { page, course = "" } = options;
		let searchStr = "";
		searchStr = this.addToQuery(page, "page", searchStr);
		searchStr = this.addToQuery(course, "course", searchStr);
		return searchStr;
	};
	addToQuery = (val, name, str) =>
		val ? (str ? `${str}&${name}=${val}` : `${name}=${val}`) : str;
	render() {
		let { data } = this.state;
		let { page, items, totalItems, totalNum } = data;
		let end = page * 3;
		let start = end - 2;
		if (end > totalNum) {
			end = totalNum;
		}
		let queryParams = queryString.parse(this.props.location.search);
		return (
			<div className='row mt-4'>
				<div className='col-1'></div>
				<div className='col-3 ml-5'>
					<LeftPanel
						courses={this.props.courses}
						options={queryParams}
						onOptionChange={this.handleOptionChange}
					/>
				</div>
				<div className='col-7'>
					<h3>All Faculties</h3>
					<h5>
						{start} - {end} of {totalNum}
					</h5>
					<div className='row bg-light py-2'>
						<div className='col-4'>Id</div>
						<div className='col-4'>Name</div>
						<div className='col-4'>Courses</div>
					</div>
					{items &&
						items.map((ele) => {
							return (
								<div className='row bg-warning border-top broder-bottom py-2'>
									<div className='col-4'>{ele.id}</div>
									<div className='col-4'>{ele.name}</div>
									<div className='col-4' style={{ overflowWrap: "break-word" }}>
										{ele.courses.map((ele) => {
											return (
												<React.Fragment>
													{ele}
													<br></br>
												</React.Fragment>
											);
										})}
									</div>
								</div>
							);
						})}
					{start === 1 ? (
						""
					) : (
						<button
							className='btn btn-secondary mt-2'
							onClick={() => this.handlePage(-1)}
						>
							Prev
						</button>
					)}
					{end === totalNum ? (
						""
					) : (
						<button
							className='btn btn-secondary float-right mt-2'
							onClick={() => this.handlePage(1)}
						>
							Next
						</button>
					)}
				</div>
			</div>
		);
	}
}
export default AllFaculties;
