import React, { Component } from "react";
import { Link, Route, Switch, Redirect } from "react-router-dom";
import http from "./httpService";
import auth from "./authService";
import Login from "./login";
import LoginAdmin from "./loginAdmin";
import Register from "./adminRegister";
import AllStudents from "./adminAllStudents";
import AllFaculties from "./adminAllFaculties";
import AssignStudent from "./adminAssignStudent";
import AssignFaculty from "./adminAssignFaculty";
import Logout from "./logout";
import LoginStudent from "./studentLogin";
import AllCourses from "./studentAllCourses";
import AllClasses from "./studentAllClasses";
import StudentDetails from "./studentDetails";
import LoginFaculty from "./loginFaculty";
import CourseAssignedFaculty from "./facultyCoursesAssigned";
import FacultyAllScheduledClasses from "./facultyScheduledClasses";
import ScheduleClass from "./facultyScheduleClass";
import NotAllowed from "./notAllowed";

class MainComponent extends Component {
	state = {
		data: {},
	};
	async componentDidMount() {
		let response = await http.get(`/getCourses`);
		let { data } = response;
		this.setState({ data: data });
	}
	render() {
		let { data } = this.state;
		let user = auth.getUser();
		return (
			<React.Fragment>
				<nav className='navbar navbar-expand-md bg-success navbar-light'>
					<Link className='navbar-brand' to='/' style={{ fontSize: "25px" }}>
						{user
							? user.role === "student"
								? "Student Home"
								: user.role === "faculty"
								? "Faculty Home"
								: "Home"
							: "Home"}
					</Link>
					<ul className='navbar-nav'>
						{user && user.role === "admin" ? (
							<React.Fragment>
								<li className='nav-item'>
									<Link className='nav-link' to='/register'>
										Register
									</Link>
								</li>
								<li className='nav-item dropdown'>
									<Link
										className='nav-link dropdown-toggle'
										data-toggle='dropdown'
									>
										Assign
									</Link>
									<div className='dropdown-menu'>
										<Link className='dropdown-item' to='/studentCourse'>
											Student to Course
										</Link>
										<Link className='dropdown-item' to='/facultyCourse'>
											Faculty to Course
										</Link>
									</div>
								</li>
								<li className='nav-item dropdown'>
									<Link
										className='nav-link dropdown-toggle'
										data-toggle='dropdown'
									>
										View
									</Link>
									<div className='dropdown-menu'>
										<Link className='dropdown-item' to='/allStudents?page=1'>
											All Students
										</Link>
										<Link className='dropdown-item' to='/allFaculties?page=1'>
											All Faculties
										</Link>
									</div>
								</li>
							</React.Fragment>
						) : user && user.role === "student" ? (
							<React.Fragment>
								<li className='nav-item'>
									<Link className='nav-link' to='/studentDetails'>
										Student Details
									</Link>
								</li>
								<li className='nav-item'>
									<Link className='nav-link' to='/allClasses'>
										All Classes
									</Link>
								</li>
								<li className='nav-item'>
									<Link className='nav-link' to='/courseStudent'>
										All Courses
									</Link>
								</li>
							</React.Fragment>
						) : user && user.role === "faculty" ? (
							<React.Fragment>
								<li className='nav-item'>
									<Link className='nav-link' to='/coursesAssigned'>
										Courses
									</Link>
								</li>
								<li className='nav-item dropdown'>
									<Link
										className='nav-link dropdown-toggle'
										data-toggle='dropdown'
									>
										Class Details
									</Link>
									<div className='dropdown-menu'>
										<Link className='dropdown-item' to='/scheduleClass'>
											Schedule a class
										</Link>
										<Link className='dropdown-item' to='/scheduledClasses'>
											All Scheduled classes
										</Link>
									</div>
								</li>
							</React.Fragment>
						) : (
							""
						)}
					</ul>
					<ul className='ml-auto navbar-nav'>
						{user ? (
							user.role === "admin" ? (
								<li className='nav-item'>
									<a className='nav-link'>Welcome Admin</a>
								</li>
							) : (
								<li className='nav-item'>
									<a className='nav-link'>Welcome {user.name}</a>
								</li>
							)
						) : (
							""
						)}
						<li className='nav-item'>
							{!user ? (
								<Link className='nav-link' to='/login'>
									Login
								</Link>
							) : (
								<Link className='nav-link' to='/logout'>
									Logout
								</Link>
							)}
						</li>
					</ul>
				</nav>

				<Switch>
					<Route path='/notAllowed' component={NotAllowed} />
					<Route
						path='/scheduleClass'
						render={(props) =>
							user ? (
								user.role === "faculty" ? (
									<ScheduleClass {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/scheduledClasses'
						render={(props) =>
							user ? (
								user.role === "faculty" ? (
									<FacultyAllScheduledClasses {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/coursesAssigned'
						render={(props) =>
							user ? (
								user.role === "faculty" ? (
									<CourseAssignedFaculty {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/faculty'
						render={(props) =>
							user ? (
								user.role === "faculty" ? (
									<LoginFaculty {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/studentDetails'
						render={(props) =>
							user ? (
								user.role === "student" ? (
									<StudentDetails {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/allClasses'
						render={(props) =>
							user ? (
								user.role === "student" ? (
									<AllClasses {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/courseStudent'
						render={(props) =>
							user ? (
								user.role === "student" ? (
									<AllCourses {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/student'
						render={(props) =>
							user ? (
								user.role === "student" ? (
									<LoginStudent {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/facultyCourse'
						render={(props) =>
							user ? (
								user.role === "admin" ? (
									<AssignFaculty {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/studentCourse'
						render={(props) =>
							user ? (
								user.role === "admin" ? (
									<AssignStudent {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/allFaculties'
						render={(props) =>
							user ? (
								user.role === "admin" ? (
									<AllFaculties {...props} courses={data} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/allStudents'
						render={(props) =>
							user ? (
								user.role === "admin" ? (
									<AllStudents {...props} courses={data} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/register'
						render={(props) =>
							user ? (
								user.role === "admin" ? (
									<Register {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/admin'
						render={(props) =>
							user ? (
								user.role === "admin" ? (
									<LoginAdmin {...props} />
								) : (
									<Redirect to='/notAllowed' />
								)
							) : (
								<Redirect to='/login' />
							)
						}
					/>
					<Route
						path='/login'
						render={(props) =>
							user ? <Redirect to='/notAllowed' /> : <Login {...props} />
						}
					/>
					<Route
						path='/logout'
						render={(props) =>
							!user ? <Redirect to='/login' /> : <Logout {...props} />
						}
					/>
					<Redirect
						from='/'
						to={
							user
								? user.role === "admin"
									? "/admin"
									: user.role === "faculty"
									? "/faculty"
									: "/student"
								: "/login"
						}
					/>
				</Switch>
			</React.Fragment>
		);
	}
}
export default MainComponent;
