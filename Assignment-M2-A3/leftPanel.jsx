import React, { Component } from "react";
import http from "./httpService";

class LeftPanel extends Component {
	state = { allCourses: this.props.courses };
	makeCbs = (arr, selArr, name) => {
		return (
			<React.Fragment>
				{arr.map((ele) => {
					return (
						<div className='col-9 border p-2 ml-4'>
							<input
								type='checkbox'
								className='form-check-input ml-2'
								name={name}
								value={ele.name}
								checked={selArr.indexOf(ele.name) >= 0 ? true : false}
								onChange={this.handleChange}
							></input>
							<label className='form-check-label ml-4'>{ele.name}</label>
						</div>
					);
				})}
			</React.Fragment>
		);
	};
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let options = { ...this.props.options };
		let { course = "" } = options;
		let coursesArr = [];
		if (course) {
			coursesArr = course.split(",");
		}
		if (input.checked) {
			coursesArr.push(input.value);
		} else {
			let index = coursesArr.indexOf(input.value);
			coursesArr.splice(index, 1);
		}
		options.course = coursesArr.join(",");
		this.props.onOptionChange(options);
	};
	render() {
		let { allCourses } = this.state;
		let { course = "" } = this.props.options;
		let coursesArr = [];
		if (course) {
			coursesArr = course.split(",");
		}
		return (
			<div className='container ml-5'>
				<div className='row'>
					<div className='col-9 bg-light border p-2 ml-4'>
						<h5 className='font-weight-bold'>Option</h5>
					</div>
					{this.makeCbs(allCourses, coursesArr, "course")}
				</div>
			</div>
		);
	}
}
export default LeftPanel;
