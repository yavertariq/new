import React, { Component } from "react";
import http from "./httpService";
import auth from "./authService";

class AllCourses extends Component {
	state = { data: {} };
	async componentDidMount() {
		let user = auth.getUser();
		let response = await http.get(`/getStudentCourse/${user.name}`);
		let { data } = response;
		this.setState({ data: data });
	}
	render() {
		let { data } = this.state;
		return (
			<div className='row mt-4'>
				<div className='col-2'></div>
				<div className='col-9'>
					<h4 style={{ marginLeft: "-15px" }}>Courses Assigned</h4>
					<div className='row bg-light font-weight-bold py-3 border-top border-bottom'>
						<div className='col-2'>Course Id</div>
						<div className='col-3'>Course Name</div>
						<div className='col-3'>Course Code</div>
						<div className='col-4'>Description</div>
					</div>
					{data.length > 0 ? (
						<React.Fragment>
							{data.map((ele) => {
								return (
									<div
										className='row py-2 border-top border-bottom'
										style={{ background: "#b1ebd2" }}
									>
										<div className='col-2'>{ele.courseId}</div>
										<div className='col-3'>{ele.name}</div>
										<div className='col-3'>{ele.code}</div>
										<div className='col-4'>{ele.description}</div>
									</div>
								);
							})}
						</React.Fragment>
					) : (
						""
					)}
				</div>
			</div>
		);
	}
}
export default AllCourses;
