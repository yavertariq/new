import React, { Component } from "react";
import auth from "./authService";
import http from "./httpService";

class Login extends Component {
	state = {
		user: {},
		errors: { email: "pop", password: "pop" },
		check: true,
	};
	async postData(url, obj) {
		try {
			let response = await http.post(url, obj);
			let { data } = response;
			auth.login(data);
			data.role === "admin"
				? (window.location = "/admin")
				: data.role === "student"
				? (window.location = "/student")
				: (window.location = "/faculty");
		} catch (ex) {
			if (ex.response && ex.response.status === 500) {
				let error = "Login Unsuccessful. Please Check the entered details.";
				this.setState({ loginError: error });
			}
		}
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.user[input.name] = input.value;
		let error = this.validateInput(input.value, input.name);
		s1.errors[input.name] = error;
		if (this.validateErrors(s1.errors)) {
			s1.check = false;
		} else {
			s1.check = true;
		}
		this.setState(s1);
	};
	validateInput = (val, name) => {
		let error = "";
		if (name === "email") {
			error = val ? "" : "Email cannot be empty";
		} else {
			error = val
				? val.length > 6
					? ""
					: "Password must be atleast 7 characters"
				: "Password must be atleast 7 characters";
		}
		return error;
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			console.log(acc);
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		this.postData("/login", s1.user);
	};
	render() {
		let { user, errors, check, loginError = "" } = this.state;
		let { email = "", password = "" } = user;
		return (
			<div className='row'>
				<div className='col-10 text-center mt-3'>
					<h4>Login</h4>
					{loginError && <span className='text-danger'>{loginError}</span>}
					<div className='row form-group'>
						<div className='col-3 text-right'>
							<h5 className='mr-3'>
								Email<span className='text-danger'>*</span>
							</h5>
						</div>
						<div className='col-7 ml-5'>
							<input
								className='form-control'
								name='email'
								value={email}
								placeholder=' Enter your email'
								onChange={this.handleChange}
							></input>
							<span className='text-muted'>
								We'll never share your email with anyone else
							</span>
						</div>
					</div>
					<div className='row form-group'>
						<div className='col-3 text-right'>
							<h5>
								Password<span className='text-danger'>*</span>
							</h5>
						</div>

						<div className='col-7 ml-5'>
							<input
								className='form-control'
								type='password'
								name='password'
								value={password}
								placeholder=' Enter Password'
								onChange={this.handleChange}
							></input>
							{errors.password && errors.password !== "pop" && (
								<span
									className='form-control'
									style={{ background: "pink", color: "#a5002f" }}
								>
									{errors.password}
								</span>
							)}
						</div>
					</div>
					<button
						className='btn btn-primary'
						disabled={check}
						onClick={this.handleSubmit}
					>
						Login
					</button>
				</div>
			</div>
		);
	}
}
export default Login;
