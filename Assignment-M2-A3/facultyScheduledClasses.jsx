import React, { Component } from "react";
import http from "./httpService";
import auth from "./authService";

class FacultyAllScheduledClasses extends Component {
	state = {
		data: {},
		view: 1,
		details: {},
		courses: [],
		errors: { course: "", time: "", endTime: "", topic: "" },
	};
	async componentDidMount() {
		let user = auth.getUser();
		let s1 = { ...this.state };
		let response = await http.get(`/getFacultyClass/${user.name}`);
		let { data } = response;
		s1.data = data;
		response = await http.get(`/getFacultyCourse/${user.name}`);
		s1.courses = response.data;
		this.setState(s1);
	}
	async putData(url, obj) {
		let response = await http.put(url, obj);
		let s1 = { ...this.state };
		s1.view = 1;
		this.setState(s1);
	}
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.details[input.name] = input.value;
		s1.errors = this.validateInput(s1.details);
		this.setState(s1);
	};
	validateInput = (details) => {
		let errors = {};
		let { course = "", time = "", endTime = "", topic = "" } = details;
		errors.course = this.checkInput(course);
		errors.time = this.checkInput(time);
		errors.endTime = this.checkInput(endTime);
		errors.topic = this.checkInput(topic);
		return errors;
	};
	checkInput = (val) => (val ? "" : "Mandatory Field");
	handleEdit = (index) => {
		let s1 = { ...this.state };
		s1.view = 2;
		s1.details = s1.data[index];
		this.setState(s1);
	};
	makeDD = (arr, name, selVal, defaultVal) => {
		return (
			<select
				className='form-control'
				name={name}
				value={selVal}
				onChange={this.handleChange}
			>
				<option value=''>Select {defaultVal}</option>
				{arr.map((ele) => {
					return <option>{ele.name}</option>;
				})}
			</select>
		);
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		if (this.validateErrors(s1.errors)) {
			this.putData(`/postClass/${s1.details.classId}`, s1.details);
		} else alert(`Enter all the details`);
	};
	addNewClass = () => {
		this.props.history.push(`/scheduleClass`);
	};
	render() {
		let { data, view, details, courses } = this.state;
		return (
			<div className='row mt-4'>
				<div className='col-2'></div>
				<div className='col-9'>
					{view === 1 ? (
						<React.Fragment>
							<h4>All Scheduled Classes</h4>
							<div className='row bg-light font-weight-bold py-3 border-top border-bottom'>
								<div className='col-4'>Course Name</div>
								<div className='col-2'>Start Time</div>
								<div className='col-2'>End Time</div>
								<div className='col-2'>Topic</div>
								<div className='col-2'></div>
							</div>
							{data.length > 0 ? (
								<React.Fragment>
									{data.map((ele, index) => {
										return (
											<div
												className='row py-2 border-top border-bottom'
												style={{ background: "#fff4aa" }}
											>
												<div className='col-4'>{ele.course}</div>
												<div className='col-2'>{ele.time}</div>
												<div className='col-2'>{ele.endTime}</div>
												<div className='col-2'>{ele.topic}</div>
												<div className='col-2'>
													<button
														className='btn btn-secondary'
														onClick={() => this.handleEdit(index)}
													>
														Edit
													</button>
												</div>
											</div>
										);
									})}
									<button
										className='btn btn-primary mt-3'
										onClick={() => this.addNewClass()}
									>
										Add New Class
									</button>
								</React.Fragment>
							) : (
								""
							)}
						</React.Fragment>
					) : (
						<div className='container'>
							<h3>Edit Details of class</h3>
							<div className='form-group'>
								{courses.length > 0 &&
									this.makeDD(courses, "course", details.course, "Course")}
							</div>
							<div className='form-group'>
								<label className='font-weight-bold'>
									Time<span className='text-danger'>*</span>
								</label>
								<input
									type='time'
									className='form-control'
									name='time'
									value={details.time}
									onChange={this.handleChange}
								></input>
							</div>
							<div className='form-group'>
								<label className='font-weight-bold'>
									End Time<span className='text-danger'>*</span>
								</label>
								<input
									type='time'
									className='form-control'
									name='endTime'
									value={details.endTime}
									onChange={this.handleChange}
								></input>
							</div>
							<div className='form-group'>
								<label className='font-weight-bold'>
									Topic<span className='text-danger'>*</span>
								</label>
								<input
									className='form-control'
									name='topic'
									placeholder=' Enter class topic'
									value={details.topic}
									onChange={this.handleChange}
								></input>
							</div>
							<button
								className='btn btn-primary mt-3'
								onClick={this.handleSubmit}
							>
								Schedule
							</button>
						</div>
					)}
				</div>
			</div>
		);
	}
}
export default FacultyAllScheduledClasses;
