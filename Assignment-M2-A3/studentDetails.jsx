import React, { Component } from "react";
import http from "./httpService";
import auth from "./authService";

class StudentDetails extends Component {
	state = {
		user: {},
		date: ["", "", ""],
		edit: false,
		errors: { gender: "pop", year: "pop", month: "pop", day: "pop" },
	};
	async componentDidMount() {
		let user = auth.getUser();
		let response = await http.get(`/getStudentDetails/${user.name}`);
		let { data } = response;
		let s1 = { ...this.state };
		if (data.gender === "" || data.dob === "") {
			s1.edit = true;
		} else {
			s1.date = data.dob.split("-");
		}
		s1.user = data;
		this.setState(s1);
	}
	async postData(url, obj) {
		let response = await http.post(url, obj);
		this.props.history.push("/student");
	}

	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (s1.edit) {
			if (
				input.name === "day" ||
				input.name === "month" ||
				input.name === "year"
			) {
				if (input.name === "day") {
					s1.date[0] = input.value;
				} else if (input.name === "month") {
					s1.date[1] = input.value;
				} else {
					s1.date[2] = input.value;
				}
				s1.user["dob"] = s1.date.join("-");
			} else s1.user[input.name] = input.value;
		}
		if (!(input.name === "about"))
			s1.errors[input.name] = this.validateInput(input.value);
		this.setState(s1);
	};
	validateInput = (val) => (val ? "" : "Mandatory Field");
	makeDD = (arr, sel, name) => {
		return (
			<select
				className='form-control'
				value={sel}
				name={name}
				onChange={this.handleChange}
				onBlur={() => this.checkDD(name)}
			>
				<option>Select {name}</option>
				{arr.map((ele) => {
					return <option>{ele}</option>;
				})}
			</select>
		);
	};
	checkDD = (name) => {
		let s1 = { ...this.state };
		let error = "";
		if (name === "day") {
			error = s1.date[0] ? "" : "Day is required";
		} else if (name === "month") {
			error = s1.date[1] ? "" : "Month is required";
		} else {
			error = s1.date[2] ? "" : "Year is required";
		}
		s1.errors[name] = error;

		this.setState(s1);
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	makeString = (user) => {
		let keys = Object.keys(user);
		let arr = keys.map((ele) => {
			let tempStr = "";
			tempStr += `"${ele}":`;
			if (ele === "id") {
				tempStr += `${user[ele]}`;
			} else {
				tempStr += `"${user[ele]}"`;
			}
			return tempStr;
		});
		let str = arr.join(",");
		str = "{" + str + "}";
		return str;
	};
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		if (this.validateErrors(s1.errors)) {
			let str = this.makeString(s1.user);
			alert(`${str} added`);
			this.postData("/postStudentDetails", s1.user);
		} else {
			alert("Enter the mandatory details");
		}
	};
	render() {
		let { user, date, errors, edit } = this.state;
		let { gender, about } = user;
		let years = [];
		for (let i = 1980; i <= 2021; i++) {
			years.push(i);
		}
		let months = [
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December",
		];
		let months31 = [
			"January",
			"March",
			"May",
			"July",
			"August",
			"October",
			"December",
		];

		let months30 = ["April", "June", "September", "November"];
		let daysNum =
			months31.indexOf(date[1]) >= 0
				? 31
				: months30.indexOf(date[1]) >= 0
				? 30
				: 0;
		if (daysNum === 0) {
			if (date[2] % 4 === 0) {
				daysNum = 29;
			} else daysNum = 28;
		}

		let days = [];
		for (let i = 1; i <= daysNum; i++) {
			days.push(i);
		}
		return (
			<div className='row mt-4'>
				<div className='col-2'></div>
				<div className='col-8'>
					<h3>Student Details</h3>
					<div className='container'>
						<label
							className='form-check-label '
							style={{ fontSize: "17px", marginRight: "100px" }}
						>
							Gender<span className='text-danger'>*</span>
						</label>
						<div className='form-check form-check-inline'>
							<input
								className='form-check-input'
								type='radio'
								name='gender'
								value='male'
								onChange={this.handleChange}
								checked={gender === "male" ? true : false}
							></input>
							<label
								className='form-check-label'
								style={{ marginRight: "180px" }}
							>
								Male
							</label>
							<input
								className='form-check-input'
								type='radio'
								name='gender'
								value='female'
								onChange={this.handleChange}
								checked={gender === "female" ? true : false}
							></input>
							<label className='form-check-label'>Female</label>
						</div>
						<hr></hr>
						<div className='form-group'>
							<label className='font-weight-bold' style={{ fontSize: "19px" }}>
								Date of Birth<span className='text-danger'>*</span>
							</label>
							<div className='row'>
								<div className='col-4'>
									{this.makeDD(years, date[2], "year")}
								</div>
								<div className='col-4'>
									{this.makeDD(months, date[1], "month")}
								</div>
								<div className='col-4'>{this.makeDD(days, date[0], "day")}</div>
								<div className='col-4'>
									{errors.year && errors.year !== "pop" && (
										<span
											className='form-control'
											style={{ background: "pink", color: "#a5002f" }}
										>
											{errors.year}
										</span>
									)}
								</div>
								<div className='col-4'>
									{errors.month && errors.month !== "pop" && (
										<span
											className='form-control'
											style={{ background: "pink", color: "#a5002f" }}
										>
											{errors.month}
										</span>
									)}
								</div>
								<div className='col-4'>
									{errors.day && errors.day !== "pop" && (
										<span
											className='form-control'
											style={{ background: "pink", color: "#a5002f" }}
										>
											{errors.day}
										</span>
									)}
								</div>
							</div>
						</div>
						<div className='form-group mt-5'>
							<label className='' style={{ fontSize: "19px" }}>
								About Myself
							</label>
							<br></br>
							<textarea
								className='col-12'
								rows='6'
								name='about'
								onChange={this.handleChange}
								value={about}
							></textarea>
						</div>
						{edit && (
							<button className='btn btn-primary' onClick={this.handleSubmit}>
								Add Details
							</button>
						)}
					</div>
				</div>
			</div>
		);
	}
}
export default StudentDetails;
