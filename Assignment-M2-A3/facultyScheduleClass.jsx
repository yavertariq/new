import React, { Component } from "react";
import http from "./httpService";
import auth from "./authService";

class ScheduleClass extends Component {
	state = {
		courses: [],
		schedClass: {},
		errors: { course: "pop", time: "pop", endTime: "pop", topic: "pop" },
	};
	async componentDidMount() {
		let user = auth.getUser();
		let s1 = { ...this.state };
		let response = await http.get(`/getFacultyCourse/${user.name}`);
		let { data } = response;
		console.log(data);
		s1.courses = data;
		s1.schedClass["facultyName"] = user.name;

		this.setState(s1);
	}
	async postData(url, obj) {
		let response = await http.post(url, obj);
		this.props.history.push(`/scheduledClasses`);
	}
	validateInput = (schedClass) => {
		let errors = {};
		console.log(schedClass);
		let { course = "", time = "", endTime = "", topic = "" } = schedClass;
		errors.course = this.checkInput(course);
		errors.time = this.checkInput(time);
		errors.endTime = this.checkInput(endTime);
		errors.topic = this.checkInput(topic);
		return errors;
	};
	checkInput = (val) => (val ? "" : "Mandatory Field");
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.schedClass[input.name] = input.value;
		s1.errors = this.validateInput(s1.schedClass);
		this.setState(s1);
	};
	makeDD = (arr, name, selVal, defaultVal) => {
		return (
			<select
				className='form-control'
				name={name}
				value={selVal}
				onChange={this.handleChange}
			>
				<option value=''>Select {defaultVal}</option>
				{arr.map((ele) => {
					return <option>{ele.name}</option>;
				})}
			</select>
		);
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		if (this.validateErrors(s1.errors)) {
			this.postData(`/postClass`, s1.schedClass);
		} else alert(`Enter all the details`);
	};
	render() {
		let { courses, schedClass } = this.state;
		let { course = "", time = "", endTime = "", topic = "" } = schedClass;
		return (
			<div className='row mt-4'>
				<div className='col-2'></div>
				<div className='col-8'>
					<h3>Schedule a class</h3>
					<div className='form-group'>
						{courses.length > 0 &&
							this.makeDD(courses, "course", course, "Course")}
					</div>
					<div className='form-group'>
						<label className='font-weight-bold'>
							Time<span className='text-danger'>*</span>
						</label>
						<input
							type='time'
							className='form-control'
							name='time'
							value={time}
							onChange={this.handleChange}
						></input>
					</div>
					<div className='form-group'>
						<label className='font-weight-bold'>
							End Time<span className='text-danger'>*</span>
						</label>
						<input
							type='time'
							className='form-control'
							name='endTime'
							value={endTime}
							onChange={this.handleChange}
						></input>
					</div>
					<div className='form-group'>
						<label className='font-weight-bold'>
							Topic<span className='text-danger'>*</span>
						</label>
						<input
							className='form-control'
							name='topic'
							placeholder=' Enter class topic'
							onChange={this.handleChange}
						></input>
					</div>
					<button className='btn btn-primary mt-3' onClick={this.handleSubmit}>
						Schedule
					</button>
				</div>
			</div>
		);
	}
}
export default ScheduleClass;
