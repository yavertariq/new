import React, { Component } from "react";
import http from "./httpService";
import auth from "./authService";

class AllClasses extends Component {
	state = { data: {} };
	async componentDidMount() {
		let user = auth.getUser();
		let response = await http.get(`/getStudentClass/${user.name}`);
		let { data } = response;
		this.setState({ data: data });
	}
	render() {
		let { data } = this.state;
		return (
			<div className='row mt-4'>
				<div className='col-2'></div>
				<div className='col-9'>
					<h4>All Scheduled Classes</h4>
					<div className='row bg-light font-weight-bold py-3 border-top border-bottom'>
						<div className='col-3'>Course Name</div>
						<div className='col-2'>Start Time</div>
						<div className='col-2'>End Time</div>
						<div className='col-3'>Faculty Name</div>
						<div className='col-2'>Topic</div>
					</div>
					{data.length > 0 ? (
						<React.Fragment>
							{data.map((ele) => {
								return (
									<div
										className='row py-2 border-top border-bottom'
										style={{ background: "#fff4aa" }}
									>
										<div className='col-3'>{ele.course}</div>
										<div className='col-2'>{ele.time}</div>
										<div className='col-2'>{ele.endTime}</div>
										<div className='col-3'>{ele.facultyName}</div>
										<div className='col-2'>{ele.topic}</div>
									</div>
								);
							})}
						</React.Fragment>
					) : (
						""
					)}
				</div>
			</div>
		);
	}
}
export default AllClasses;
