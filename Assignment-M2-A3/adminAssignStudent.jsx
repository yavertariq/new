import React, { Component } from "react";
import http from "./httpService";

class AssignStudent extends Component {
	state = {
		data: {},
		view: 1,
		details: {},
		students: [],
		errors: {},
	};
	async componentDidMount() {
		let response = await http.get("/getCourses");
		let { data } = response;
		let s1 = { ...this.state };
		s1.data = data;
		response = await http.get(`/getStudentNames`);
		s1.students = response.data;
		this.setState(s1);
	}
	async putData(url, obj) {
		let response = await http.put(url, obj);
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.componentDidMount();
	}
	makeCBs = (arr, selArr, name) => {
		return (
			<React.Fragment>
				{arr.map((ele) => {
					return (
						<div className='form-check'>
							<input
								type='checkbox'
								className='form-check-input'
								name={name}
								value={ele}
								checked={selArr.indexOf(ele) >= 0 ? true : false}
								onChange={this.handleChange}
							></input>
							<label className='form-check-label'>{ele}</label>
						</div>
					);
				})}
			</React.Fragment>
		);
	};
	handleEdit = (index) => {
		let s1 = { ...this.state };
		s1.view = 2;
		s1.details = s1.data[index];
		this.setState(s1);
	};
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		if (input.type === "checkbox") {
			if (input.checked) {
				s1.details[input.name].push(input.value);
			} else {
				let index = s1.details[input.name].indexOf(input.value);
				s1.details[input.name].splice(index, 1);
			}
		} else s1.details[input.name] = input.value;
		if (
			input.name === "name" ||
			input.name === "code" ||
			input.name === "students"
		) {
			let error = this.validateInput(s1.details[input.name], input.name);
			s1.errors[input.name] = error;
		}
		this.setState(s1);
	};
	validateInput = (value, name) => {
		let error = "";
		if (name === "name" || name === "code") {
			error = value ? "" : "Cannot be Empty";
		} else {
			error = value.length > 0 ? "" : "Atleast one student required";
		}
		return error;
	};
	validateErrors = (errors) => {
		let keys = Object.keys(errors);
		let count = keys.reduce((acc, curr) => {
			if (errors[curr]) acc = acc + 1;
			return acc;
		}, 0);
		return count === 0;
	};
	handleSubmit = (e) => {
		e.preventDefault();
		let s1 = { ...this.state };
		if (this.validateErrors(s1.errors)) {
			this.putData(`/putCourse`, s1.details);
			s1.view = 1;
			this.setState(s1);
		}
	};
	render() {
		let { data, view, details, students } = this.state;
		return (
			<div className='container mt-5'>
				{data.length > 0 && view === 1 ? (
					<React.Fragment>
						<h3>Add students to a course</h3>
						<div className='row bg-light font-weight-bold'>
							<div className='col-1'>Courseid</div>
							<div className='col-3'>Name</div>
							<div className='col-2'>Course Code</div>
							<div className='col-3'>Description</div>
							<div className='col-2'>Students</div>
							<div className='col-1'></div>
						</div>
						{data.map((ele, index) => {
							return (
								<div className='row bg-warning border-top border-bottom'>
									<div className='col-1'>{ele.courseId}</div>
									<div className='col-3'>{ele.name}</div>
									<div className='col-2'>{ele.code}</div>
									<div className='col-3'>{ele.description}</div>
									<div className='col-2'>
										{ele.students.map((ele) => {
											return (
												<React.Fragment>
													{ele}
													<br></br>
												</React.Fragment>
											);
										})}
									</div>
									<div className='col-1'>
										<button
											className='btn btn-secondary'
											onClick={() => this.handleEdit(index)}
										>
											Edit
										</button>
									</div>
								</div>
							);
						})}
					</React.Fragment>
				) : (
					<React.Fragment>
						<h4>Add students to a course</h4>
						<hr></hr>
						<h3>Edit the course</h3>
						<div className='form-group'>
							<label>Name</label>
							<input
								className='form-control'
								name='name'
								value={details.name}
								onChange={this.handleChange}
							></input>
						</div>
						<div className='form-group'>
							<label>Course Code</label>
							<input
								className='form-control'
								name='name'
								value={details.code}
								onChange={this.handleChange}
							></input>
						</div>
						<div className='form-group'>
							<label>Description</label>
							<input
								className='form-control'
								name='name'
								value={details.description}
								onChange={this.handleChange}
							></input>
						</div>
						<div className='form-group'>
							<label className='font-weight-bold'>
								Students<span className='text-danger'>*</span>
							</label>
							{this.makeCBs(students, details.students, "students")}
							<button className='btn btn-primary' onClick={this.handleSubmit}>
								Update
							</button>
						</div>
					</React.Fragment>
				)}
			</div>
		);
	}
}
export default AssignStudent;
