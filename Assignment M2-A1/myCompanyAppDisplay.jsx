import React, { Component } from "react";
import queryString from "query-string";
import LeftPanel from "./myCompanyAppLeftPanel";

class Display extends Component {
	state = {
		emps: [
			{
				name: "Amit Kumar",
				department: "Finance",
				designation: "Manager",
				salary: 24000,
				email: "amit.kumar@company.com",
				mobile: "9898346473",
				location: "New Delhi",
			},
			{
				name: "Preeti Sharma",
				department: "Technology",
				designation: "Manager",
				salary: 28500,
				email: "preeti.sharma@company.com",
				mobile: "9898236541",
				location: "New Delhi",
			},
			{
				name: "Vishal Verma",
				department: "Operations",
				designation: "Manager",
				salary: 22100,
				email: "vishal.verma@company.com",
				mobile: "9910346632",
				location: "New Delhi",
			},
			{
				name: "Charu Kumari",
				department: "HR",
				designation: "Manager",
				salary: 23500,
				email: "charu.kumari@company.com",
				mobile: "7023734553",
				location: "New Delhi",
			},
			{
				name: "Puneet Gupta",
				department: "Finance",
				designation: "Trainee",
				salary: 14450,
				email: "puneet.gupta@company.com",
				mobile: "8836436731",
				location: "Noida",
			},
			{
				name: "Namita Singh",
				department: "Technology",
				designation: "Trainee",
				salary: 14590,
				email: "namita.singh@company.com",
				mobile: "9801228812",
				location: "Noida",
			},
			{
				name: "Samit Bansal",
				department: "Operations",
				designation: "Trainee",
				salary: 13900,
				email: "samit.bansal@company.com",
				mobile: "7003551118",
				location: "Noida",
			},
			{
				name: "Priya Talwar",
				department: "HR",
				designation: "Trainee",
				salary: 14450,
				email: "priya.talwar@company.com",
				mobile: "814452341",
				location: "Noida",
			},
			{
				name: "Shivam Singh",
				department: "Finance",
				designation: "Trainee",
				salary: 15100,
				email: "shivam.singh@company.com",
				mobile: "7173958440",
				location: "Noida",
			},
			{
				name: "Shelja Prasad",
				department: "Technology",
				designation: "Trainee",
				salary: 15500,
				email: "shelja.prasad@company.com",
				mobile: "9898346473",
				location: "Noida",
			},
			{
				name: "Mithali Dutt",
				department: "Finance",
				designation: "President",
				salary: 68200,
				email: "mithali.dutt@company.com",
				mobile: "98100346731",
				location: "New Delhi",
			},
			{
				name: "Pradeep Kumar",
				department: "Technology",
				designation: "President",
				salary: 84900,
				email: "pradeep.kumar@company.com",
				mobile: "98254634121",
				location: "New Delhi",
			},
			{
				name: "Amit Singh",
				department: "Operations",
				designation: "President",
				salary: 71250,
				email: "amit.singh@company.com",
				mobile: "98145537842",
				location: "New Delhi",
			},
			{
				name: "Garima Rai",
				department: "HR",
				designation: "President",
				salary: 69200,
				email: "garima.rai@company.com",
				mobile: "998107654387",
				location: "New Delhi",
			},
		],
	};
	handlePage = (val) => {
		let queryParams = queryString.parse(this.props.location.search);
		let { page = 1 } = queryParams;
		let newPage = +page + val;
		queryParams.page = newPage;
		this.changeURL("/emps", queryParams);
	};
	changeURL = (url, options) => {
		let { loc } = this.props.match.params;
		let { page, department, designation } = options;
		let searchStr = "";
		if (loc) {
			url += `/${loc}`;
			searchStr = this.addToQueryString(searchStr, "page", page);
			searchStr = this.addToQueryString(searchStr, "department", department);
			searchStr = this.addToQueryString(searchStr, "designation", designation);
		} else {
			searchStr = this.addToQueryString(searchStr, "page", page);
			searchStr = this.addToQueryString(searchStr, "department", department);
			searchStr = this.addToQueryString(searchStr, "designation", designation);
		}
		this.props.history.push({ pathname: url, search: searchStr });
	};
	handleOptions = (options) => {
		this.changeURL("/emps", options);
	};

	addToQueryString = (str, paramName, paramValue) =>
		paramValue
			? str
				? `${str}&${paramName}=${paramValue}`
				: `${paramName}=${paramValue}`
			: str;

	render() {
		let { emps } = this.state;
		let designations = emps.reduce((acc, curr) => {
			if (acc.indexOf(curr.designation) < 0) acc.push(curr.designation);
			return acc;
		}, []);
		let departments = emps.reduce((acc, curr) => {
			if (acc.indexOf(curr.department) < 0) acc.push(curr.department);
			return acc;
		}, []);
		let arr = emps;
		let { loc } = this.props.match.params;
		if (loc) {
			arr = emps.filter((ele) => {
				return ele.location === loc;
			});
		}
		let queryParams = queryString.parse(this.props.location.search);
		let { page = 1, department, designation } = queryParams;
		let startIndex = page - 1;
		let endIndex = page;
		if (department) {
			arr = arr.filter((ele) => {
				return department.indexOf(ele.department) >= 0;
			});
		}
		if (designation) {
			arr = arr.filter((ele) => {
				return ele.designation === designation;
			});
		}
		let arr2 = arr.filter((ele, index) => {
			return index >= startIndex && index <= endIndex;
		});

		return (
			<div className='container'>
				<div className='row'>
					<div className='col-3'>
						<LeftPanel
							designations={designations}
							departments={departments}
							options={queryParams}
							onChangeOption={this.handleOptions}
						/>
					</div>
					<div className='col-9'>
						<h4 className='text-center'>Welcome to the Employee Portal</h4>
						<h6>You have Chosen</h6>
						Location: {loc ? loc : "All"}
						<br />
						Department: {department ? department : "All"}
						<br />
						Designation: {designation ? designation : "All"}
						<br />
						<br />
						The number of employees matching the options: {arr.length}
						<div className='row'>
							{arr2.map((ele) => {
								return (
									<div className='col-6 border bg-light'>
										<h6>{ele.name}</h6>
										{ele.email}
										<br />
										Mobile: {ele.mobile}
										<br />
										Location: {ele.location}
										<br />
										Department: {ele.department}
										<br />
										Designation: {ele.designation}
										<br />
										Salary: {ele.salary}
										<br />
									</div>
								);
							})}
						</div>
						<div className='row'>
							{startIndex > 0 ? (
								<div className='col-1'>
									<button
										className='btn btn-primary'
										onClick={() => this.handlePage(-1)}
									>
										Prev
									</button>
								</div>
							) : (
								""
							)}
							<div className='col-10'></div>
							{endIndex < arr.length - 1 ? (
								<div className='col-1'>
									<button
										className='btn btn-primary'
										onClick={() => this.handlePage(1)}
									>
										Next
									</button>
								</div>
							) : (
								""
							)}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default Display;
