import React, { Component } from "react";
import { Link, Route, Switch, Redirect } from "react-router-dom";
import Display from "./myCompanyAppDisplay";

class MainComponent extends Component {
	render() {
		return (
			<div className='container'>
				<nav className='navbar navbar-expand-md navbar-dark bg-dark'>
					<Link className='navbar-brand' to='/'>
						MyPortal
					</Link>
					<ul className='navbar-nav'>
						<li>
							<Link to='/emps' className='nav-link' key='products'>
								All
							</Link>
						</li>

						<li>
							<Link to='/emps/New Delhi' className='nav-link' key='newProduct'>
								New Delhi
							</Link>
						</li>
						<li>
							<Link to='/emps/Noida' className='nav-link' key='newProduct'>
								Noida
							</Link>
						</li>
					</ul>
				</nav>
				<Switch>
					<Route path='/emps/:loc' component={Display} />
					<Route path='/emps' component={Display} />
					<Redirect from='/' to='/emps' />
				</Switch>
			</div>
		);
	}
}
export default MainComponent;
