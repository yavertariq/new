import React, { Component } from "react";

class LeftPanel extends Component {
	makeRadios = (arr, sel, name, label) => {
		return (
			<React.Fragment>
				<label className='form-check-label font-weight-bold'>{label}</label>
				{arr.map((ele) => {
					return (
						<div className='form-check'>
							<input
								className='form-check-input'
								type='radio'
								name={name}
								value={ele}
								checked={sel === ele}
								onChange={this.handleChange}
							></input>
							<label className='form-check-label'>{ele}</label>
						</div>
					);
				})}
			</React.Fragment>
		);
	};
	makeCBs = (arr, name, sel, label) => {
		return (
			<React.Fragment>
				<label className='form-check-label font-weight-bold'>{label}</label>
				{arr.map((ele) => {
					return (
						<div className='form-check' key={ele}>
							<input
								className='form-check-input'
								type='checkbox'
								name={name}
								value={ele}
								checked={sel.indexOf(ele) >= 0 ? true : false}
								onChange={this.handleChange}
							></input>
							<label className='form-check-label'>{ele}</label>
						</div>
					);
				})}
			</React.Fragment>
		);
	};
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let options = { ...this.props.options };
		if (input.type === "checkbox") {
			options[input.name] = this.updateCBs(
				options[input.name],
				input.checked,
				input.value
			);
		} else {
			options[input.name] = input.value;
		}
		this.props.onChangeOption(options);
	};
	updateCBs = (str, check, value) => {
		let inpArr = str ? str.split(",") : [];
		if (check) inpArr.push(value);
		else {
			let index = inpArr.findIndex((ele) => {
				return ele === value;
			});
			if (index >= 0) inpArr.splice(index, 1);
		}
		return inpArr.join(",");
	};

	render() {
		let { designations, departments, options } = this.props;
		let { department = "", designation = "" } = options;
		return (
			<React.Fragment>
				<div className='form-check'>
					{this.makeRadios(
						designations,
						designation,
						"designation",
						"Designation"
					)}
				</div>
				<div className='form-check'>
					{this.makeCBs(
						departments,
						"department",
						department.split(","),
						"Department"
					)}
				</div>
			</React.Fragment>
		);
	}
}
export default LeftPanel;
