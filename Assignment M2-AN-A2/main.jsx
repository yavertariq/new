import React, { Component } from "react";
import { Link, NavLink, Route, Switch } from "react-router-dom";
import Display from "./display";
import Home from "./home";
import MyShelf from "./myShelf";
import Settings from "./settings";

class MainComponent extends Component {
	state = {
		active: "",
		myBooks: [],
		checks: {
			printType: false,
			filter: false,
			orderBy: false,
			languages: false,
			maxResults: 8,
		},
	};
	changeActive = (val) => {
		this.setState({ active: val });
	};
	handleMyBooks = (books) => {
		let s1 = { ...this.state };
		s1.myBooks = books;
		this.setState(s1);
	};
	handleSettings = (checks) => {
		this.setState({ checks: checks });
	};
	render() {
		let { active, checks } = this.state;
		return (
			<React.Fragment>
				<nav className='navbar navbar-expand-sm navbar-light bg-light'>
					<Link className='navbar-brand' to='/'>
						<i class='fas fa-book-open fa-2x'></i>
					</Link>
					<ul className='navbar-nav'>
						<li className='nav-item'>
							<NavLink
								className='nav-link font-weight-bold'
								onClick={() => this.changeActive("Harry Potter")}
								to={`/books?searchText=Harry Potter&startIndex=0&maxResults=${checks.maxResults}`}
								activeStyle={active === "Harry Potter" ? { color: "blue" } : ""}
								style={{ color: "black" }}
							>
								Harry Potter
							</NavLink>
						</li>
						<li className='nav-item'>
							<NavLink
								className='nav-link font-weight-bold'
								to={`/books?searchText=Agatha Christie&startIndex=0&maxResults=${checks.maxResults}`}
								onClick={() => this.changeActive("Agatha Christie")}
								activeStyle={
									active === "Agatha Christie" ? { color: "blue" } : ""
								}
								style={{ color: "black" }}
							>
								Agatha Christie
							</NavLink>
						</li>
						<li className='nav-item'>
							<NavLink
								className='nav-link font-weight-bold'
								onClick={() => this.changeActive("Premchand")}
								to={`/books?searchText=Premchand&startIndex=0&maxResults=${checks.maxResults}`}
								activeStyle={active === "Premchand" ? { color: "blue" } : ""}
								style={{ color: "black" }}
							>
								Premchand
							</NavLink>
						</li>
						<li className='nav-item'>
							<NavLink
								className='nav-link font-weight-bold'
								onClick={() => this.changeActive("Jane Austin")}
								to={`/books?searchText=Jane Austin&startIndex=0&maxResults=${checks.maxResults}`}
								activeStyle={active === "Jane Austin" ? { color: "blue" } : ""}
								style={{ color: "black" }}
							>
								Jane Austin
							</NavLink>
						</li>
						<li className='nav-item'>
							<NavLink
								className='nav-link font-weight-bold'
								to='/myshelf'
								activeStyle={{ color: "blue" }}
								style={{ color: "black" }}
							>
								My Books
							</NavLink>
						</li>
						<li className='nav-item'>
							<NavLink
								className='nav-link font-weight-bold'
								to='/settings'
								activeStyle={{ color: "blue" }}
								style={{ color: "black" }}
							>
								Settings
							</NavLink>
						</li>
					</ul>
				</nav>
				<Switch>
					<Route
						path='/settings'
						render={(props) => (
							<Settings
								{...props}
								onChangeSettings={this.handleSettings}
								checks={checks}
							/>
						)}
					/>
					<Route
						path='/myshelf'
						render={(props) => (
							<MyShelf
								{...props}
								onAddToMyBooks={this.handleMyBooks}
								myBooks={this.state.myBooks}
							/>
						)}
					/>
					<Route
						path='/books'
						render={(props) => (
							<Display
								{...props}
								onAddToMyBooks={this.handleMyBooks}
								myBooks={this.state.myBooks}
								checks={checks}
							/>
						)}
					/>
					<Route
						path='/'
						render={(props) => (
							<Home {...props} maxResults={checks.maxResults} />
						)}
					/>
				</Switch>
			</React.Fragment>
		);
	}
}
export default MainComponent;
