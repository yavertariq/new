import React, { Component } from "react";

class MyShelf extends Component {
	state = {
		myBooks: this.props.myBooks,
	};
	removeFromMyBooks = (id) => {
		let s1 = { ...this.state };
		let bookIndex = s1.myBooks.findIndex((ele) => {
			return ele.id === id;
		});
		s1.myBooks.splice(bookIndex, 1);
		this.setState(s1);
		this.props.onAddToMyBooks(s1.myBooks);
	};
	render() {
		let { myBooks } = this.state;
		return (
			<React.Fragment>
				<div className='bg-info text-warning text-center'>
					<h3>
						{myBooks.length === 0 ? "No Books Added to My Books" : "My Books"}
					</h3>
				</div>
				<div className='container'>
					<div className='row'>
						{myBooks.map((ele) => {
							return (
								<div className='col-3 bg-success border text-center'>
									<img
										className='img-fluid'
										src={
											ele.volumeInfo.imageLinks
												? ele.volumeInfo.imageLinks.thumbnail
												: ""
										}
									></img>
									<br></br>
									<b>{ele.volumeInfo.title}</b>
									<br />
									{ele.volumeInfo.authors
										? ele.volumeInfo.authors.join(", ")
										: ""}
									<br></br>
									{ele.volumeInfo.categories
										? ele.volumeInfo.categories.join(", ")
										: "NA"}
									<br></br>
									<button
										className='btn btn-secondary m-5'
										onClick={() => this.removeFromMyBooks(ele.id)}
									>
										Remove From MyBooks
									</button>
								</div>
							);
						})}
					</div>
				</div>
			</React.Fragment>
		);
	}
}
export default MyShelf;
