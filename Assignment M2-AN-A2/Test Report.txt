Test Report:

Implemented Navbar.
Issue: Color of active nav-link does not change to blue.
Issue resolved using NavLink instead of Link.
Issue:when one link was clicked multiple links got highlighted.
Issue resolved.

Implemented the home page with the image and search bar and button.
no issue.

Implemented main books display.
no issue.

Implemented Search bar functionality.
no issue.

Implemented the left panel.
no issue.

Implemented functionality of left Panel.
Issue:was not filtering data even though the queryParams were showing correctly.
Issue resolved.

Paginations Implemented.
Issue: The value show NaN when the prev button was clicked.
Issue resolved.

Implmented Add to My Books.
Issue: Books added did not show remove button after clicking add button.
Issue resolved.
Issue: Books added show remove button but when tabs are changed from navbar the books again show add button even though they are already added.
Issue resolved.

Implemented My Books page.
no issue.

Implemented settings page .
no issue.

Implemented Filter according to settings.
no issues.

everything working as intended.



API's used and tested in postman:

https://www.googleapis.com/books/v1/volumes?q=SherlockHolmes
https://www.googleapis.com/books/v1/volumes?q=SherlockHolmes&filter=partial
https://www.googleapis.com/books/v1/volumes?q=SherlockHolmes&filter=full
https://www.googleapis.com/books/v1/volumes?q=SherlockHolmes&filter=full&startIndex=0&maxResults=4
https://www.googleapis.com/books/v1/volumes?q=SherlockHolmes&filter=full&startIndex=0&maxResults=4&printType=magazines
https://www.googleapis.com/books/v1/volumes?q=SherlockHolmes&filter=full&startIndex=0&maxResults=4&langRestrict=fr&printType=magazines
https://www.googleapis.com/books/v1/volumes?q=Jane Austin&langRestrict=hi



 