import React, { Component } from "react";

class Home extends Component {
	state = { search: "" };
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.search = input.value;
		this.setState(s1);
	};

	handleSearch = () => {
		let s1 = { ...this.state };
		this.props.history.push(
			`/books?searchText=${s1.search}&startIndex=0&maxResults=${this.props.maxResults}`
		);
	};
	render() {
		let { search } = this.state;
		return (
			<React.Fragment>
				<div className='container text-center'>
					<img
						src={
							"https://www.detroitlabs.com/wp-content/uploads/2018/02/alfons-morales-YLSwjSy7stw-unsplash.jpg"
						}
						className='rounded-circle m-4'
						style={{ height: "300px", width: "300px" }}
					></img>
				</div>
				<div className='row'>
					<div className='col-6 ' style={{ marginLeft: "200px" }}>
						<input
							className='form-control'
							name={search}
							value={search}
							placeholder=' Search'
							onChange={this.handleChange}
						></input>
					</div>
					<button
						className='btn btn-primary'
						onClick={() => this.handleSearch()}
					>
						Search
					</button>
				</div>
			</React.Fragment>
		);
	}
}
export default Home;
