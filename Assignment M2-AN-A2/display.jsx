import React, { Component } from "react";
import http from "./serverGoogleBooks";
import queryString from "query-string";
import LeftPanel from "./leftPanel";

class Display extends Component {
	state = {
		data: {},
		myBooks: this.props.myBooks,
		ids: [],
	};
	async fetchData() {
		let queryParams = queryString.parse(this.props.location.search);
		let str = this.makeSearchString(queryParams);
		let response = await http.get(`?q=${str}`);
		let { data } = response;
		let ids = this.props.myBooks.map((ele) => {
			return ele.id;
		});
		this.setState({ data: data, ids: ids });
	}
	componentDidMount() {
		this.fetchData();
	}
	async componentDidUpdate(prevProps, prevState) {
		if (prevProps !== this.props) this.fetchData();
	}
	makeSearchString = (queryParams) => {
		let {
			searchText,
			startIndex = 0,
			maxResults = 8,
			langRestrict,
			filter,
			orderBy,
			printBy,
		} = queryParams;
		let searchStr = searchText;
		searchStr = this.addQueryToString(searchStr, "startIndex", startIndex);
		searchStr = this.addQueryToString(searchStr, "maxResults", maxResults);
		searchStr = this.addQueryToString(searchStr, "langRestrict", langRestrict);
		searchStr = this.addQueryToString(searchStr, "filter", filter);
		searchStr = this.addQueryToString(searchStr, "orderBy", orderBy);
		searchStr = this.addQueryToString(searchStr, "printBy", printBy);
		return searchStr;
	};
	addQueryToString = (str, name, value) =>
		value ? `${str}&${name}=${value}` : str;
	getSearchStr = (str) => {
		let queryParams = queryString.parse(this.props.location.search);
		let { searchText, startIndex, maxResults } = queryParams;
		this.props.history.push(
			`/books?searchText=${searchText}&startIndex=${startIndex}&maxResults=${maxResults}${str}`
		);
	};
	changePage = (check) => {
		let queryParams = queryString.parse(this.props.location.search);
		let { startIndex, maxResults } = queryParams;
		let newIndex = 0;
		check === "Next"
			? (newIndex = +startIndex + parseInt(maxResults))
			: (newIndex = +startIndex - parseInt(maxResults));
		queryParams.startIndex = newIndex.toString();

		let str = this.makeSearchString(queryParams);
		this.props.history.push(`/books?searchText=${str}`);
	};
	addToMyBooks = (id) => {
		let s1 = { ...this.state };
		let book = s1.data.items.find((ele) => {
			return ele.id === id;
		});
		s1.myBooks.push(book);
		s1.ids.push(book.id);
		this.setState(s1);
		this.props.onAddToMyBooks(s1.myBooks);
	};
	removeFromMyBooks = (id) => {
		let s1 = { ...this.state };
		let bookIndex = s1.myBooks.findIndex((ele) => {
			return ele.id === id;
		});
		s1.myBooks.splice(bookIndex, 1);
		s1.ids.splice(bookIndex, 1);
		this.setState(s1);
		this.props.onAddToMyBooks(s1.myBooks);
	};
	render() {
		let { data, myBooks, ids } = this.state;
		let queryParams = queryString.parse(this.props.location.search);
		let { startIndex, maxResults } = queryParams;
		let endIndex = +startIndex + parseInt(maxResults);
		let { searchText } = queryParams;
		let { items = [] } = data;
		return (
			<div className='row'>
				<div className='col-3'>
					<LeftPanel
						onOptionChange={this.getSearchStr}
						checks={this.props.checks}
					/>
				</div>
				<div className='col-9 text-center'>
					<h3 className='text-center text-warning'>{searchText} Books</h3>
					<br></br>
					<h4 className='text-success text-left'>
						{+startIndex + 1} to {endIndex}
					</h4>
					{data ? (
						<div className='row'>
							{items.map((ele) => {
								console.log(myBooks.indexOf(ele), myBooks);
								return (
									<div className='col-3 bg-success border text-center'>
										<img
											className='img-fluid'
											src={
												ele.volumeInfo.imageLinks
													? ele.volumeInfo.imageLinks.thumbnail
													: ""
											}
										></img>
										<br></br>
										<b>{ele.volumeInfo.title}</b>
										<br />
										{ele.volumeInfo.authors
											? ele.volumeInfo.authors.join(", ")
											: ""}
										<br></br>
										{ele.volumeInfo.categories
											? ele.volumeInfo.categories.join(", ")
											: "NA"}
										<br></br>
										{ids.indexOf(ele.id) < 0 ? (
											<button
												className='btn btn-secondary m-5'
												onClick={() => this.addToMyBooks(ele.id)}
											>
												Add to MyBooks
											</button>
										) : (
											<button
												className='btn btn-secondary m-5'
												onClick={() => this.removeFromMyBooks(ele.id)}
											>
												Remove From MyBooks
											</button>
										)}
									</div>
								);
							})}
						</div>
					) : (
						""
					)}
					{startIndex > 1 ? (
						<button
							className='btn btn-warning float-left m-3'
							onClick={() => this.changePage("Prev")}
						>
							Prev
						</button>
					) : (
						""
					)}
					<button
						className='btn btn-warning float-right m-3'
						onClick={() => this.changePage("Next")}
					>
						Next
					</button>
				</div>
			</div>
		);
	}
}
export default Display;
