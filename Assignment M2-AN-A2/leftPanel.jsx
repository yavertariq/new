import React, { Component } from "react";

class LeftPanel extends Component {
	state = {
		selVals: { langRestrict: "", filter: "", printType: "", orderBy: "" },
	};
	makeRadios = (arr, sel, name, label) => {
		return (
			<React.Fragment>
				<div className='bg-light border p-1'>
					<label className='form-check-label font-weight-bold ml-3'>
						{label}
					</label>
				</div>
				{arr.map((ele) => {
					return (
						<div className='form-check border p-2'>
							<input
								className='form-check-input ml-1'
								type='radio'
								name={name}
								value={ele}
								checked={sel === ele}
								onChange={this.handleChange}
							></input>
							<label className='form-check-label ml-4'>
								{ele === "en"
									? "English"
									: ele === "fr"
									? "French"
									: ele === "hi"
									? "Hindi"
									: ele === "es"
									? "Spanish"
									: ele === "zh"
									? "Chinese"
									: ele}
							</label>
						</div>
					);
				})}
			</React.Fragment>
		);
	};
	makeDD = (arr, name, selVal, defaultVal) => {
		return (
			<select
				className='form-control'
				name={name}
				value={selVal}
				onChange={this.handleChange}
			>
				<option value=''>Select {defaultVal}</option>
				{arr.map((ele) => {
					return <option>{ele}</option>;
				})}
			</select>
		);
	};
	makeSearch = () => {
		let { printType, filter, langRestrict, orderBy } = this.state.selVals;
		let searchStr = "";
		searchStr = this.addQueryToString(searchStr, "langRestrict", langRestrict);
		searchStr = this.addQueryToString(searchStr, "filter", filter);
		searchStr = this.addQueryToString(searchStr, "orderBy", orderBy);
		searchStr = this.addQueryToString(searchStr, "printType", printType);
		this.props.onOptionChange(searchStr);
	};
	addQueryToString = (str, name, value) =>
		value ? `${str}&${name}=${value}` : str;
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		s1.selVals[input.name] = input.value;
		this.setState(s1);
		this.makeSearch();
	};
	render() {
		let languages = ["en", "fr", "hi", "es", "zh"];
		let filters = ["partial", "full", "free-ebooks", "paid-ebooks"];
		let printTypes = ["all", "books", "magazines"];
		let orderBys = ["relevance", "newest"];
		let { langRestrict, printType, filter, orderBy } = this.state.selVals;
		let { checks } = this.props;
		return (
			<div className='container text-left'>
				<hr></hr>
				{!checks.languages
					? this.makeRadios(languages, langRestrict, "langRestrict", "Language")
					: ""}
				<hr></hr>
				{!checks.filter
					? this.makeRadios(filters, filter, "filter", "Filter")
					: ""}
				<hr></hr>
				{!checks.printType
					? this.makeRadios(printTypes, printType, "printType", "Print Type")
					: ""}
				<hr></hr>
				{!checks.orderBy
					? this.makeDD(orderBys, "orderBy", orderBy, " Order By")
					: ""}
			</div>
		);
	}
}
export default LeftPanel;
