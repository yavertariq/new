import React, { Component } from "react";

class Settings extends Component {
	state = {
		checks: this.props.checks,
	};
	handleChange = (e) => {
		let { currentTarget: input } = e;
		let s1 = { ...this.state };
		console.log(input.checked);
		input.type === "checkbox"
			? input.checked
				? (s1.checks[input.name] = true)
				: (s1.checks[input.name] = false)
			: (s1.checks[input.name] = input.value);
		this.setState(s1);
		this.props.onChangeSettings(this.state.checks);
	};
	render() {
		let { checks } = this.state;
		let { printType, filter, languages, maxResults, orderBy } = checks;
		return (
			<div className='mt-5 text-left ml-3'>
				<h5 className='text-danger'>
					Select Options for Filtering on Left Panel
				</h5>
				<div className='form-check'>
					<input
						type='checkbox'
						className='form-check-input'
						name='printType'
						checked={printType}
						onChange={this.handleChange}
					></input>
					<label className='form-check-label'>
						printType --(Restrict to books or magazines)
					</label>
				</div>
				<div className='form-check'>
					<input
						type='checkbox'
						className='form-check-input'
						name='languages'
						checked={languages}
						onChange={this.handleChange}
					></input>
					<label className='form-check-label'>
						languages --(Restrict the volumes returned to those that are tagged
						with the specified language)
					</label>
				</div>
				<div className='form-check'>
					<input
						type='checkbox'
						className='form-check-input'
						name='filter'
						checked={filter}
						onChange={this.handleChange}
					></input>
					<label className='form-check-label'>
						filter --(Filter search results by volume type and availability)
					</label>
				</div>
				<div className='form-check'>
					<input
						type='checkbox'
						className='form-check-input'
						name='orderBy'
						checked={orderBy}
						onChange={this.handleChange}
					></input>
					<label className='form-check-label'>
						orderBy --(Order of the volume search result)
					</label>
				</div>
				<div className='form-group'>
					<h4 className='text-success'>No of Entries on a page</h4>
					<input
						className='form-control col-4'
						name='maxResults'
						value={maxResults}
						onChange={this.handleChange}
					></input>
				</div>
			</div>
		);
	}
}
export default Settings;
